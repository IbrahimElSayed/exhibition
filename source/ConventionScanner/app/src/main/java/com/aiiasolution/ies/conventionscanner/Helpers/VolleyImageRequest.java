package com.aiiasolution.ies.conventionscanner.Helpers;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Custom implementation of Volley Request Queue
 *
 * By Ibrahim El Sayed
 *
 */

public class VolleyImageRequest {

    private static VolleyImageRequest mInstance;
    private static Context mCtx;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;


    //USAGE !

    /*

        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        **THE_WANTED_IMAGE_VIEW**.setImageUrl(**THE_WANTED_IMAGE_VIEW_URL**, mImageLoader);

     */

    private VolleyImageRequest(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized VolleyImageRequest getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyImageRequest(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(mCtx.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            // Don't forget to start the volley request queue
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }


    /*private VolleyImageListener mVolleyImageListener;
    public interface VolleyImageListener {void onRequestImageUrl(boolean response);}
    public void setVolleyImageListener(VolleyImageListener mVolleyImageListener){this.mVolleyImageListener = mVolleyImageListener;}*/

}