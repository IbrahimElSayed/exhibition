package com.aiiasolution.ies.conventionscanner.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothDeviceRecord;
import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.Models.Beacon;
import com.aiiasolution.ies.conventionscanner.NoteActivity;
import com.aiiasolution.ies.conventionscanner.SharedContactsActivity;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothScannerThreadBaseClass;
import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothScannerThreadSdkVersionMin19Max20;
import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothScannerThreadSdkVersionMin21;
import com.aiiasolution.ies.conventionscanner.MainActivity;
import com.aiiasolution.ies.conventionscanner.Models.Companies;
import com.aiiasolution.ies.conventionscanner.Models.DevicesController;
import com.aiiasolution.ies.conventionscanner.Models.Rooms;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.example.ies.conventionscanner.R;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPOutputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class mainFragment extends Fragment implements BluetoothScannerThreadBaseClass.BlueToothListener {

    public BluetoothScannerThreadBaseClass bluetoothScannerThread;

    /* ---- private properties ---- */
    private View mainView;
    private static final Integer MAX_BEACONS = 1;
    private ImageLoader mImageLoader;

    private ArrayList<DevicesController> AllDevicesControllers = new ArrayList<>();
    private ArrayList<Rooms> nearbyRooms = new ArrayList<>();
    private ArrayList<String> urlDL = new ArrayList<>();

    private SimpleDateFormat simpleDateFormat;
    private String dateTimeString;
    private String IMEI;
    private String trajectoryID;

    private TextView tvWebsite;
    private TextView tvCatalogue;
    private LinearLayout llNowRom;

    private String mFirstScannedName;
    private Integer ChosenView = 0;
    /* ---- End private properties ---- */

    public mainFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(mainView == null) {
            mainView = inflater.inflate(R.layout.fragment_main, container, false);
            initViews();
            runBlueToothInit();
        }else initViews();
        return mainView;
    }
    @Override
    public void onResume()
    {
        if(AllDevicesControllers.size() >= 1) {
            if(MainActivity.ALL_COMPANIES_FAV.containsKey(AllDevicesControllers.get(0).getCompany().getID())) {
                mainView.findViewById(R.id.icallcommands).findViewById(R.id.ivHeartIcon).setAlpha(0.20f);
                MainActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
                MainActivity.favBtn.setText(String.valueOf(MainActivity.mfavBtnCount));
                if (MainActivity.favBtn.getVisibility() == View.GONE
                        && MainActivity.ALL_COMPANIES_FAV.size() >= 1)
                    MainActivity.favBtn.setVisibility(View.VISIBLE);
            }
        }
        super.onResume();
    }

    private void initViews()
    {
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.lladdbrochure).setOnClickListener(bottomBarHandler);
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.lladdmeeting).setOnClickListener(bottomBarHandler);
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.llmakefav).setOnClickListener(bottomBarHandler);
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.llsharecontact).setOnClickListener(bottomBarHandler);
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.llshowvideo).setOnClickListener(bottomBarHandler);
        mainView.findViewById(R.id.icallcommands).findViewById(R.id.lltakenote).setOnClickListener(bottomBarHandler);

        llNowRom = (LinearLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inNowRoom).findViewById(R.id.llNowRoomLayout);
        Animation animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.jump);
        llNowRom.setAnimation(animation1);

        TextView tvApproximateMainRoom = (TextView) mainView.findViewById(R.id.tvMainRoom);
        NetworkImageView tvApproximateMainRoomImage = (NetworkImageView) mainView.findViewById(R.id.tvMainRoomImage);
        ProgressBar pbApproximateMainRoomSpinner = (ProgressBar) mainView.findViewById(R.id.pbMainSpinner);
        tvWebsite = (TextView) mainView.findViewById(R.id.tvWebsite);
        tvCatalogue = (TextView) mainView.findViewById(R.id.tvCatalogue);

        //final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        //int paramsHeightWidth = (int) (50 * scale + 0.5f);
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(paramsHeightWidth, paramsHeightWidth);

        TextView tvNearbyRoom0 = (TextView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inNowRoom).findViewById(R.id.tvApproximateRoom);
        NetworkImageView ivNearbyRoom0 = (NetworkImageView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inNowRoom).findViewById(R.id.ivApproximateImage);
        ProgressBar pbNearbyRoom0 = (ProgressBar) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inNowRoom).findViewById(R.id.pbApproximateSpinner);
        FrameLayout flRoomNotifier0 = (FrameLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inNowRoom).findViewById(R.id.flMainRoomNotifier);
        //ivNearbyRoom0.setLayoutParams(params);
        //pbNearbyRoom0.setLayoutParams(params);

        TextView tvNearbyRoom1 = (TextView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFirstRoom).findViewById(R.id.tvApproximateRoom);
        NetworkImageView ivNearbyRoom1 = (NetworkImageView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFirstRoom).findViewById(R.id.ivApproximateImage);
        ProgressBar pbNearbyRoom1 = (ProgressBar) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFirstRoom).findViewById(R.id.pbApproximateSpinner);
        FrameLayout flRoomNotifier1 = (FrameLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFirstRoom).findViewById(R.id.flMainRoomNotifier);

        TextView tvNearbyRoom2 = (TextView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inSecondRoom).findViewById(R.id.tvApproximateRoom);
        NetworkImageView ivNearbyRoom2 = (NetworkImageView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inSecondRoom).findViewById(R.id.ivApproximateImage);
        ProgressBar pbNearbyRoom2 = (ProgressBar) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inSecondRoom).findViewById(R.id.pbApproximateSpinner);
        FrameLayout flRoomNotifier2 = (FrameLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inSecondRoom).findViewById(R.id.flMainRoomNotifier);

        TextView tvNearbyRoom3 = (TextView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inThirdRoom).findViewById(R.id.tvApproximateRoom);
        NetworkImageView ivNearbyRoom3 = (NetworkImageView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inThirdRoom).findViewById(R.id.ivApproximateImage);
        ProgressBar pbNearbyRoom3 = (ProgressBar) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inThirdRoom).findViewById(R.id.pbApproximateSpinner);
        FrameLayout flRoomNotifier3 = (FrameLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inThirdRoom).findViewById(R.id.flMainRoomNotifier);

        TextView tvNearbyRoom4 = (TextView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFourthRoom).findViewById(R.id.tvApproximateRoom);
        NetworkImageView ivNearbyRoom4 = (NetworkImageView) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFourthRoom).findViewById(R.id.ivApproximateImage);
        ProgressBar pbNearbyRoom4 = (ProgressBar) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFourthRoom).findViewById(R.id.pbApproximateSpinner);
        FrameLayout flRoomNotifier4 = (FrameLayout) mainView.findViewById(R.id.icCloseStands).findViewById(R.id.inFourthRoom).findViewById(R.id.flMainRoomNotifier);

        nearbyRooms.clear();
        nearbyRooms.add(new Rooms(tvApproximateMainRoom, tvApproximateMainRoomImage, pbApproximateMainRoomSpinner, null));
        nearbyRooms.add(new Rooms(tvNearbyRoom0, ivNearbyRoom0, pbNearbyRoom0, flRoomNotifier0));
        nearbyRooms.add(new Rooms(tvNearbyRoom2, ivNearbyRoom2, pbNearbyRoom2, flRoomNotifier2));
        nearbyRooms.add(new Rooms(tvNearbyRoom3, ivNearbyRoom3, pbNearbyRoom3, flRoomNotifier3));
        nearbyRooms.add(new Rooms(tvNearbyRoom1, ivNearbyRoom1, pbNearbyRoom1, flRoomNotifier1));
        nearbyRooms.add(new Rooms(tvNearbyRoom4, ivNearbyRoom4, pbNearbyRoom4, flRoomNotifier4));

        final float scale = getContext().getResources().getDisplayMetrics().density;
        int pixels = (int) (50 * scale + 0.5f);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pixels, pixels);
        ivNearbyRoom0.setLayoutParams(params); pbNearbyRoom0.setLayoutParams(params);

        pixels = (int) (40 * scale + 0.5f);
        params =new LinearLayout.LayoutParams(pixels, pixels);
        ivNearbyRoom2.setLayoutParams(params);
        ivNearbyRoom3.setLayoutParams(params);
        pbNearbyRoom2.setLayoutParams(params);
        pbNearbyRoom3.setLayoutParams(params);

        /*mainView.findViewById(R.id.icCloseStands).setOnTouchListener(new SwipeLayout(getContext()){
            @Override
            public void onSwipeLeft() {
                swipeLeft();
            }
            @Override
            public void onSwipeRight() {
                swipeRight();
            }
        });*/

        mainView.findViewById(R.id.inNowRoom).setOnClickListener(TopBarHandler);
        mainView.findViewById(R.id.inFirstRoom).setOnClickListener(TopBarHandler);
        mainView.findViewById(R.id.inSecondRoom).setOnClickListener(TopBarHandler);
        mainView.findViewById(R.id.inThirdRoom).setOnClickListener(TopBarHandler);
        mainView.findViewById(R.id.inFourthRoom).setOnClickListener(TopBarHandler);

        for(int i = 0; i < nearbyRooms.size(); i++)
        {
            nearbyRooms.get(i).getRoomImageProgress().getIndeterminateDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
        }


    }

    private void swipeLeft()
    {
        if(AllDevicesControllers.size() >= 4)
        {
            ArrayList<DevicesController> nearbyRoomsTemp = new ArrayList<>();
            nearbyRoomsTemp.addAll(AllDevicesControllers);
            if(AllDevicesControllers.size() >= 4) {
                AllDevicesControllers.set(0, nearbyRoomsTemp.get(3));
                AllDevicesControllers.set(2, nearbyRoomsTemp.get(0));
                AllDevicesControllers.set(1, nearbyRoomsTemp.get(2));
                AllDevicesControllers.set(3, nearbyRoomsTemp.get(1));
            }
            if (AllDevicesControllers.size() >= 5) {
                AllDevicesControllers.set(4, nearbyRoomsTemp.get(1));
                AllDevicesControllers.set(3, nearbyRoomsTemp.get(4));
            }
            approximateRoom();
        }
    }

    private void swipeRight()
    {
        if(AllDevicesControllers.size() >= 2)
        {
            ArrayList<DevicesController> nearbyRoomsTemp = new ArrayList<>();
            nearbyRoomsTemp.addAll(AllDevicesControllers);
            if(AllDevicesControllers.size() == 2) {
                AllDevicesControllers.set(1, nearbyRoomsTemp.get(0));
                AllDevicesControllers.set(0, nearbyRoomsTemp.get(1));
            }
            if(AllDevicesControllers.size() == 3)
            {
                AllDevicesControllers.set(1, nearbyRoomsTemp.get(0));
                AllDevicesControllers.set(2, nearbyRoomsTemp.get(1));
                AllDevicesControllers.set(0, nearbyRoomsTemp.get(2));
            }
            if(AllDevicesControllers.size() == 4)
            {
                AllDevicesControllers.set(3, nearbyRoomsTemp.get(0));
                AllDevicesControllers.set(1, nearbyRoomsTemp.get(3));
                AllDevicesControllers.set(2, nearbyRoomsTemp.get(1));
                AllDevicesControllers.set(0, nearbyRoomsTemp.get(2));
            }
            if(AllDevicesControllers.size() >= 5)
            {
                AllDevicesControllers.set(3, nearbyRoomsTemp.get(0));
                AllDevicesControllers.set(4, nearbyRoomsTemp.get(3));
                AllDevicesControllers.set(1, nearbyRoomsTemp.get(4));
                AllDevicesControllers.set(2, nearbyRoomsTemp.get(1));
                AllDevicesControllers.set(0, nearbyRoomsTemp.get(2));
            }
            approximateRoom();
        }
    }

    private void addEvent(String title)
    {
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.Events.TITLE, title);
        startActivity(intent);
    }

    private void openVideo(String url)
    {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openSharedContacts()
    {
        Intent openShared = new Intent(getActivity(), SharedContactsActivity.class);
        openShared.putExtra("companyID", AllDevicesControllers.get(0).getCompany().getID());
        startActivity(openShared);
    }

    private void makeFavourite()
    {
        Gson gson = new Gson();
        if(!MainActivity.ALL_COMPANIES_FAV.containsKey(AllDevicesControllers.get(0).getCompany().getID())) {

            favourites newFav = new favourites(AllDevicesControllers.get(0).getCompany().getID(), AllDevicesControllers.get(0).getCompany(),
                    MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()));
            MainActivity.ALL_COMPANIES_FAV.put(AllDevicesControllers.get(0).getCompany().getID(), newFav);
            String favCompanyJson = gson.toJson(MainActivity.ALL_COMPANIES_FAV);
            try {
                FileOutputStream fOut = getContext().getApplicationContext().openFileOutput("favcompanies", 0);
                fOut.write(favCompanyJson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mainView.findViewById(R.id.icallcommands).findViewById(R.id.ivHeartIcon).setAlpha(0.20f);
            MainActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
            MainActivity.favBtn.setText(String.valueOf(MainActivity.mfavBtnCount));
            if(MainActivity.favBtn.getVisibility() == View.GONE
                    && MainActivity.ALL_COMPANIES_FAV.size() >= 1)
                MainActivity.favBtn.setVisibility(View.VISIBLE);
            //Toast.makeText(getApplicationContext(), "added to favourites", Toast.LENGTH_SHORT).show();
        }else
        {
            MainActivity.ALL_COMPANIES_FAV.remove(AllDevicesControllers.get(0).getCompany().getID());
            String favCompanyJson = gson.toJson(MainActivity.ALL_COMPANIES_FAV);
            try {
                FileOutputStream fOut = getContext().getApplicationContext().openFileOutput("favcompanies", 0);
                fOut.write(favCompanyJson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mainView.findViewById(R.id.icallcommands).findViewById(R.id.ivHeartIcon).setAlpha(1.0f);
            MainActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
            MainActivity.favBtn.setText(String.valueOf(MainActivity.mfavBtnCount));
            if(MainActivity.ALL_COMPANIES_FAV.size() >= 1)
            MainActivity.favBtn.setVisibility(View.VISIBLE);
            else MainActivity.favBtn.setVisibility(View.GONE);
        }
    }

    private void takeNote()
    {
        Intent intent = new Intent(getActivity(), NoteActivity.class);
        intent.putExtra("companyID", AllDevicesControllers.get(0).getCompany().getID());
        getActivity().startActivity(intent);
    }

    //Bluetooth adapter Init
    private void runBlueToothInit()
    {
        if(        getContext().checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && getContext().checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            startBluetoothScan();
        }else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getActivity().requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            else startBluetoothScan();
        }
    }
    public void startBluetoothScan()
    {
        simpleDateFormat = new SimpleDateFormat("_k_m_s__d_MM_yyyy");
        IMEI = MainActivity.telephonyManager.getDeviceId();
        dateTimeString = simpleDateFormat.format(new Date());
        trajectoryID = IMEI + "_" + dateTimeString;
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            bluetoothScannerThread = new BluetoothScannerThreadSdkVersionMin19Max20(getContext(),
                    trajectoryID);
        } else {
            bluetoothScannerThread = new BluetoothScannerThreadSdkVersionMin21(getContext()
                    , trajectoryID);
        }
        bluetoothScannerThread.SetBluetoothScanListener(this);
        bluetoothScannerThread.start();
    }


    //Bluetooth output data and utilization
    @Override
    public void ShowBluetoothData(List<BluetoothDeviceRecord> Data, Boolean FullData)
    {
        //Toast.makeText(getApplicationContext(), "Size - " + Data.size(), Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            inspectBluetoothDataOldDevices(Data);
        else {
            //Log.e("Test --- ", "Data size: " + Data.size());
            AllDevicesControllers.clear();
            ArrayList<String> AllCollectedCompanies = new ArrayList<>();
            for (BluetoothDeviceRecord SingleData : Data) {
                if (AllDevicesControllers.size() > MAX_BEACONS) break;
                for (Companies SingleComapny : MainActivity.ALL_COMPANIES.values()) {
                    if (AllDevicesControllers.size() > MAX_BEACONS) break;
                    for (Beacon SingleBeacon : SingleComapny.getRelatedBeacons()) {
                        if (SingleBeacon.getBeaconName().equals(SingleData.getAddress().toLowerCase())
                                && !AllCollectedCompanies.contains(SingleComapny.getName())) {
                            AllCollectedCompanies.add(SingleComapny.getName());
                            String DeviceRRSI = String.valueOf(SingleData.getRSSI());
                            String DeviceEstimatedRange = String.valueOf(new DecimalFormat("##.##").format(calculateDistance(SingleData.getRSSI(), 2412)));
                            String DeviceMacAddress = SingleData.getAddress();
                            String TimeStamp = String.valueOf(SingleData.getTimeStamp());
                            DevicesController newDevice = new DevicesController(SingleBeacon.getBeaconID(), SingleComapny.getName(), DeviceRRSI, DeviceEstimatedRange, DeviceMacAddress, TimeStamp,
                                    SingleComapny);
                            AllDevicesControllers.add(newDevice);
                            testDataItems(newDevice);
                            if (AllDevicesControllers.size() > MAX_BEACONS) break;
                        }
                    }
                }
            }
            AllCollectedCompanies.clear();

            Collections.sort(AllDevicesControllers, new Comparator<DevicesController>() {
                @Override
                public int compare(DevicesController o1, DevicesController o2) {
                    return Double.compare(Double.parseDouble(o1.getEstimateRange()), Double.parseDouble(o2.getEstimateRange()));
                }
            });



            //adapter.notifyDataSetChanged();
            if (AllDevicesControllers.size() >= MAX_BEACONS) {
                mFirstScannedName = AllDevicesControllers.get(0).getName();
                ChosenView = 0;
                approximateRoom();
            }
            else {

            }
        }
    }
    private void inspectBluetoothDataOldDevices(final List<BluetoothDeviceRecord> Data)
    {
        AllDevicesControllers.clear();
        ArrayList<String> AllCollectedCompanies = new ArrayList<>();
        for (BluetoothDeviceRecord SingleData : Data)
        {
            for(Companies SingleComapny : MainActivity.ALL_COMPANIES.values()) {

                if (AllDevicesControllers.size() > MAX_BEACONS) break;
                for(Beacon SingleBeacon : SingleComapny.getRelatedBeacons()){

                    if (AllDevicesControllers.size() > MAX_BEACONS) break;
                    if (SingleBeacon.getBeaconName().equals(SingleData.getAddress().toLowerCase())
                            && !AllCollectedCompanies.contains(SingleComapny.getName())) {
                        AllCollectedCompanies.add(SingleComapny.getName());
                        String DeviceRRSI = String.valueOf(SingleData.getRSSI());
                        String DeviceEstimatedRange = String.valueOf(new DecimalFormat("##.##").format(calculateDistance(SingleData.getRSSI(), 2412)));
                        String DeviceMacAddress = SingleData.getAddress();
                        String TimeStamp = String.valueOf(SingleData.getTimeStamp());
                        DevicesController newDevice = new DevicesController(SingleBeacon.getBeaconID(), SingleComapny.getName(), DeviceRRSI, DeviceEstimatedRange, DeviceMacAddress, TimeStamp,
                                SingleComapny);
                        AllDevicesControllers.add(newDevice);
                        testDataItems(newDevice);
                        if(AllDevicesControllers.size() > MAX_BEACONS) break;
                    }
                }
            }
        }
        AllCollectedCompanies.clear();

        Collections.sort(AllDevicesControllers, new Comparator<DevicesController>() {
            @Override
            public int compare(DevicesController o1, DevicesController o2) {
                return Double.compare(Double.parseDouble(o1.getEstimateRange()), Double.parseDouble(o2.getEstimateRange()));
            }
        });


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //adapter.notifyDataSetChanged();
                if(AllDevicesControllers.size() >= MAX_BEACONS) {
                    mFirstScannedName = AllDevicesControllers.get(0).getName();
                    ChosenView = 0;
                    approximateRoom();
                }
                else
                {

                }
            }
        });
    }
    private void approximateRoom()
    {
        try {

            for(int i = 0; i < nearbyRooms.size(); i++)
            {
                nearbyRooms.get(i).getRoomText().setText("scanning...");
                setImageUrl(nearbyRooms.get(i).getRoomImage(), "");
                nearbyRooms.get(i).getRoomImage().setVisibility(View.GONE);
                nearbyRooms.get(i).getRoomImageProgress().setVisibility(View.VISIBLE);
            }

            ArrayList<String> CollectedRooms = new ArrayList<>();

            if (AllDevicesControllers.get(0).getName() != null && AllDevicesControllers.get(0).getCompany().getImgURL() != null
                    && MainActivity.All_COMPANIES_INFO.containsKey(AllDevicesControllers.get(0).getCompany().getID())
                    && MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()).getWebsite() != null
                    && MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()).getCatalogue() != null) {
                nearbyRooms.get(0).getRoomText().setText(AllDevicesControllers.get(0).getName());
                if (nearbyRooms.get(0).getRoomImage().getVisibility() == View.GONE) {
                    nearbyRooms.get(0).getRoomImageProgress().setVisibility(View.GONE);
                    nearbyRooms.get(0).getRoomImage().setVisibility(View.VISIBLE);
                }
                setImageUrl(nearbyRooms.get(0).getRoomImage(), AllDevicesControllers.get(0).getCompany().getImgURL());
                tvWebsite.setText(MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()).getWebsite());
                tvCatalogue.setText(MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()).getCatalogue());
                CollectedRooms.add(AllDevicesControllers.get(0).getName());

                nearbyRooms.get(1).getRoomText().setText(AllDevicesControllers.get(0).getName());
                nearbyRooms.get(1).getRoomImageProgress().setVisibility(View.GONE);
                nearbyRooms.get(1).getRoomImage().setVisibility(View.VISIBLE);
                nearbyRooms.get(1).getRoomMainNotifier().setVisibility(View.GONE);
                setImageUrl(nearbyRooms.get(1).getRoomImage(), AllDevicesControllers.get(0).getCompany().getImgURL());

                if(MainActivity.ALL_COMPANIES_FAV.containsKey(AllDevicesControllers.get(0).getCompany().getID()))
                {
                    mainView.findViewById(R.id.ivHeartIcon).setAlpha(0.20f);
                }else mainView.findViewById(R.id.ivHeartIcon).setAlpha(1.0f);
            } else {
                setImageUrl(nearbyRooms.get(0).getRoomImage(), "");
                nearbyRooms.get(0).getRoomImageProgress().setVisibility(View.VISIBLE);
                nearbyRooms.get(0).getRoomImage().setVisibility(View.GONE);
                nearbyRooms.get(0).getRoomText().setText("Unknown");
                tvWebsite.setText("Unknown");
                tvCatalogue.setText("Unknown");

                nearbyRooms.get(1).getRoomText().setText("scanning...");
                setImageUrl(nearbyRooms.get(1).getRoomImage(), "");
                nearbyRooms.get(1).getRoomImageProgress().setVisibility(View.VISIBLE);
                nearbyRooms.get(1).getRoomImage().setVisibility(View.GONE);
                nearbyRooms.get(1).getRoomMainNotifier().setVisibility(View.GONE);

                if (AllDevicesControllers.get(0).getName() != null)
                    CollectedRooms.add(AllDevicesControllers.get(0).getName());
            }

            int j = 2;
            for (int i = 0; i < AllDevicesControllers.size(); i++) {
                if (j == nearbyRooms.size()) break;
                if (!CollectedRooms.contains(AllDevicesControllers.get(i).getName())) {
                    if (AllDevicesControllers.get(i).getName() != null && AllDevicesControllers.get(i).getCompany().getImgURL() != null) {
                        CollectedRooms.add(AllDevicesControllers.get(i).getName());
                        nearbyRooms.get(j).getRoomText().setText(AllDevicesControllers.get(i).getName());
                        nearbyRooms.get(j).getRoomImageProgress().setVisibility(View.GONE);
                        nearbyRooms.get(j).getRoomImage().setVisibility(View.VISIBLE);
                        nearbyRooms.get(j).getRoomMainNotifier().setVisibility(View.GONE);
                        setImageUrl(nearbyRooms.get(j).getRoomImage(), AllDevicesControllers.get(i).getCompany().getImgURL());
                        j++;
                    } else {
                        nearbyRooms.get(j).getRoomText().setText("scanning...");
                        setImageUrl(nearbyRooms.get(j).getRoomImage(), "");
                        nearbyRooms.get(j).getRoomImageProgress().setVisibility(View.VISIBLE);
                        nearbyRooms.get(j).getRoomImage().setVisibility(View.GONE);
                        j++;
                    }
                }
            }

            for (int i = 1; i < nearbyRooms.size(); i++)
            {
                if(nearbyRooms.get(i).getRoomText().getText().toString().equals(mFirstScannedName))
                {
                    nearbyRooms.get(i).getRoomText().setTextColor(Color.RED);
                }
                else
                {
                    nearbyRooms.get(i).getRoomText().setTextColor(Color.BLACK);
                }
            }

        }catch (Exception ex){ }
    }
    private void rearangeData(Integer ChosenView)
    {
        ArrayList<DevicesController> tempController = new ArrayList<>();
        tempController.addAll(AllDevicesControllers);
        AllDevicesControllers.set(0, tempController.get(ChosenView));
        AllDevicesControllers.set(ChosenView, tempController.get(0));
    }
    //*******************************************************************************************************************
    //Testing functions for excel sheet statistics
    private List<DevicesController> testingData = new ArrayList<>();
    private Handler mUiHandler = new Handler();
    private Boolean mUIHandlerSaving = false;
    public float getBatteryLevel() {
        Intent batteryIntent = getActivity().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float)level / (float)scale) * 100.0f;
    }
    private void testDataItems(DevicesController givenItem)
    {
        if(MainActivity.TESTING_CSV) {
            if (!mUIHandlerSaving)
                testingData.add(givenItem);

            if (testingData.size() >= MAX_BEACONS - 1 && !mUIHandlerSaving) {
                mUIHandlerSaving = true;
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Collections.sort(testingData, new Comparator<DevicesController>() {
                            @Override
                            public int compare(DevicesController o1, DevicesController o2) {
                                return o1.getTimeStamp().compareTo(o2.getTimeStamp());
                            }
                        });
                        generateBluetoothInfoCalibrationTesting(testingData);

                        testingData.clear();
                        mUIHandlerSaving = false;
                    }
                });
            }
        }
    }
    private void generateBluetoothInfoCalibrationTesting(List<DevicesController> bluetoothDeviceRecordArrayList)
    {

        final String fileName = "Bluetooth__" + trajectoryID + "__"
                + System.currentTimeMillis() + "__RESULT.csv";

        //creating directory
        File smartConnectDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.
                DIRECTORY_DOCUMENTS)+"/smartConnect/");
        //File smartConnectDirectory = getApplicationContext().getDir(fileName, Context.MODE_PRIVATE); //Creating an internal dir;
        if (!smartConnectDirectory.exists()) {
            if(!smartConnectDirectory.mkdirs()){
                //
            }
        }

        File bluetoothFile = new File(smartConnectDirectory + "/" + fileName);

        FileOutputStream bluetoothFileOutputStream;
        GZIPOutputStream gzipBluetoothFileOutputStream;

        String Header = "";

        // generate the header
        String[] cellTitles = new String[]{"Trajectory_ID", "Battery_Level", "TimeStamp", "Mac Address",
                "Device Name", "RSSI", "Distance"};

        //note: number of steps really detected is temporary for now
        for (int i = 0; i < cellTitles.length; i++) {
            Header = Header + cellTitles[i];
            if (i != cellTitles.length - 1) {
                Header = Header + ",";
            }
        }
        try {
            bluetoothFileOutputStream = new FileOutputStream(bluetoothFile);
            gzipBluetoothFileOutputStream = new GZIPOutputStream(bluetoothFileOutputStream);

            gzipBluetoothFileOutputStream.write(Header.getBytes());
            gzipBluetoothFileOutputStream.write("\n".getBytes());
            gzipBluetoothFileOutputStream.flush();


            //removeDuplicates(bluetoothDeviceRecordArrayList);//cleaning from duplicates:
            // issue with lescan on some devices

            if (!bluetoothDeviceRecordArrayList.isEmpty()) {
                String rowData;
                for (DevicesController item : bluetoothDeviceRecordArrayList) {
                    rowData = trajectoryID + ",";
                    rowData = rowData + getBatteryLevel() + ",";
                    rowData = rowData + item.toCSVRow(); //note: you need to add "\n"

                    gzipBluetoothFileOutputStream.write(rowData.getBytes());
                    gzipBluetoothFileOutputStream.write("\n".getBytes());
                }
            }

            gzipBluetoothFileOutputStream.flush();
            gzipBluetoothFileOutputStream.close();
        }catch (Exception ex){ Log.e("TEST -- ", ex.getMessage());}

    }
    //*******************************************************************************************************************
    //End of Testing


    //Methods for calculating approximate distances algorithm
    /* WIKIPEDIA extracted FREE PATH LOSS Formula, Note: -20 is inputed manually */
    public double calculateDistance(double signalLevelInDb, double freqInMHz) {
        signalLevelInDb = -30 - (signalLevelInDb);
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) + signalLevelInDb) / 20.0;
        return Math.pow(10.0, exp);
    }
    public double calculateDistanceTest(double signalLevelInDb, double freqInMHz) {
        Double Ratio = signalLevelInDb/-20;
        Double Distance = 0.0;
        if(Ratio < 1.0) Distance = Math.pow(Ratio, 10);
        else Distance = 2.992026 * (Math.pow(Ratio, 6.672908)) - 1.767203;

        return  Distance;
    }



    private void setImageUrl(NetworkImageView givenImage, String givenUrl)
    {
        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }
    private int approximateDominantRoomLocation(List<DevicesController> Rooms)
    {
        ArrayList<Integer> detectDominantNumber = new ArrayList<>();
        for(int i = 0; i < MAX_BEACONS; i++)
        {
            detectDominantNumber.add(Rooms.get(i).getID());
        }
        Integer[] RoomsIncrement = new Integer[]{0, 0, 0, 0, 0};
        for (Integer i : detectDominantNumber)
        {
            if(i == 0) RoomsIncrement[0]++;
            if(i == 1) RoomsIncrement[1]++;
            if(i == 2) RoomsIncrement[2]++;
            if(i == 3) RoomsIncrement[3]++;
            if(i == 4) RoomsIncrement[4]++;
        }
        int BiggestIndex = 0;
        int BiggestNum  =  RoomsIncrement[0];
        for(int i = 1; i < RoomsIncrement.length; i++)
        {
            if(BiggestNum < RoomsIncrement[i])
            {
                BiggestIndex = i;
                BiggestNum = RoomsIncrement[i];
            }
        }

        return BiggestIndex;
    }
    private int averageMeanRoomBeaconsDistance(List<DevicesController> Rooms)
    {
        Double[] RoomsIncrement = new Double[]{0., 0., 0., 0., 0.};
        Integer[] EnumRoomsIncrement = new Integer[]{0, 0, 0, 0, 0};
        for(int i = 0; i < Rooms.size(); i++)
        {
            /*if(Integer.parseInt(Rooms.get(i).getName().replaceAll("[^0-9]", "")) == 0){
                RoomsIncrement[0]+= Double.parseDouble(Rooms.get(i).getTxLevel());
                EnumRoomsIncrement[0]++;
            }*/
            if(Rooms.get(i).getID() == 1) {
                RoomsIncrement[1] += Double.parseDouble(Rooms.get(i).getEstimateRange());
                EnumRoomsIncrement[1]++;
            }
            if(Rooms.get(i).getID() == 2) {
                RoomsIncrement[2] += Double.parseDouble(Rooms.get(i).getEstimateRange());
                EnumRoomsIncrement[2]++;
            }
            if(Rooms.get(i).getID() == 3) {
                RoomsIncrement[3] += Double.parseDouble(Rooms.get(i).getEstimateRange());
                EnumRoomsIncrement[3]++;
            }
            if(Rooms.get(i).getID() == 4) {
                RoomsIncrement[4] += Double.parseDouble(Rooms.get(i).getEstimateRange());
                EnumRoomsIncrement[4]++;
            }
        }

        //RoomsIncrement[0] = RoomsIncrement[0] / EnumRoomsIncrement[0];
        RoomsIncrement[1] = RoomsIncrement[1] / EnumRoomsIncrement[1];
        RoomsIncrement[2] = RoomsIncrement[2] / EnumRoomsIncrement[2];
        RoomsIncrement[3] = RoomsIncrement[3] / EnumRoomsIncrement[3];
        RoomsIncrement[4] = RoomsIncrement[4] / EnumRoomsIncrement[4];

        int BiggestIndex = 1;
        Double SmallestNum  =  RoomsIncrement[1];
        for(int i = 2; i < RoomsIncrement.length; i++)
        {
            if(SmallestNum > RoomsIncrement[i])
            {
                BiggestIndex = i;
                SmallestNum = RoomsIncrement[i];
            }
        }

        return BiggestIndex;
    }
    private int communityRoomLocationVote(Integer Arg1, Integer Arg2, Integer Arg3)
    {
        int[] VoteRooms = new int[]{0,0,0,0,0};
        VoteRooms[Arg1] += 2;
        VoteRooms[Arg2] += 1;
        VoteRooms[Arg3] += 1;

        int BiggestIndex = 1;
        int BiggestNum  =  VoteRooms[1];
        for(int i = 2; i < VoteRooms.length; i++)
        {
            if(BiggestNum < VoteRooms[i])
            {
                BiggestIndex = i;
                BiggestNum = VoteRooms[i];
            }
        }

        return BiggestIndex;
    }







    View.OnClickListener bottomBarHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
            v.startAnimation(blinkAnim);
            if(AllDevicesControllers.size() >= 1) {

                switch (v.getId()) {

                    case R.id.llsharecontact:
                        openSharedContacts();
                        break;
                    case R.id.lladdmeeting:
                        addEvent(AllDevicesControllers.get(0).getCompany().getName());
                        break;
                    case R.id.lladdbrochure:
                        Toast.makeText(getContext(), AllDevicesControllers.get(0).getCompany().getName() + " brochure has been added inside your account", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.llshowvideo:
                        openVideo(MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()).getVideoURL());
                        break;
                    case R.id.llmakefav:
                        makeFavourite();
                        break;
                    case R.id.lltakenote:
                        takeNote();
                        break;
                    default: Toast.makeText(getContext(), "Under development", Toast.LENGTH_SHORT).show();break;

                }

            }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener TopBarHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
            v.startAnimation(blinkAnim);
            if(AllDevicesControllers.size() >= 1) {

                switch (v.getId()) {

                    case R.id.inNowRoom:
                        ChosenView = 0;
                        rearangeData(ChosenView);
                        approximateRoom();
                        break;
                    case R.id.inFirstRoom:
                        if(AllDevicesControllers.size() >= 4)
                        {
                            ChosenView = 3;
                            rearangeData(ChosenView);
                            approximateRoom();
                        }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.inSecondRoom:
                        if(AllDevicesControllers.size() >= 2)
                        {
                            ChosenView = 1;
                            rearangeData(ChosenView);
                            approximateRoom();
                        }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.inThirdRoom:
                        if(AllDevicesControllers.size() >= 3)
                        {
                            ChosenView = 2;
                            rearangeData(ChosenView);
                            approximateRoom();
                        }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.inFourthRoom:
                        if(AllDevicesControllers.size() >= 5)
                        {
                            ChosenView = 4;
                            rearangeData(ChosenView);
                            approximateRoom();
                        }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
                        break;
                    default: Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();break;

                }

            }else Toast.makeText(getContext(), "No company detected", Toast.LENGTH_SHORT).show();
        }
    };
}
