package com.aiiasolution.ies.conventionscanner.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.Interfaces.loadingCompleted;
import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.aiiasolution.ies.conventionscanner.NoteActivity;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aiiasolution.ies.conventionscanner.Adapters.NoteAdapter;
import com.aiiasolution.ies.conventionscanner.MainActivity;
import com.aiiasolution.ies.conventionscanner.Models.CompaniesInformation;
import com.example.ies.conventionscanner.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteFragment extends Fragment implements NoteAdapter.viewNote {


    public static HashMap<Integer, NoteModel> AllNotes = new HashMap<>();

    private View mView;
    private CompaniesInformation CompanyChosen;
    private ImageLoader mImageLoader;
    private TabLayout mtabwidget;

    private Boolean editingNote = false;
    private Integer selectedNoteID;

    private Integer[] CategoryImgs = new Integer[]{
            R.mipmap.note_lead,
            R.mipmap.note_team,
            R.mipmap.note_product,
            R.mipmap.note_later,
            R.mipmap.note_other
    };

    private LinearLayout llNotesListings;
    private ListView lvNotesLists;
    private Button btnAddNote;

    private LinearLayout llNotepad;
    private EditText noteTextToSave;
    private Button noteDiscardBtn;
    private Button noteSaveBtn;

    public NoteFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(mView == null) {
            mView = inflater.inflate(R.layout.fragment_note, container, false);
            initValues();
            initViews(mView);
        }
        new loadData().execute();
        return mView;
    }

    private void initValues()
    {
        CompanyChosen = MainActivity.All_COMPANIES_INFO.get(getActivity().getIntent().getIntExtra("companyID", -1));
    }

    private void initViews(View v)
    {
        TextView CompanyName = (TextView) v.findViewById(R.id.tvNoteCompanyName);
        NetworkImageView CompanyImg = (NetworkImageView) v.findViewById(R.id.ivNoteCompanyImg);

        llNotesListings = (LinearLayout) v.findViewById(R.id.llNotesListings);
        lvNotesLists = (ListView) v.findViewById(R.id.lvNotesList);
        btnAddNote = (Button) v.findViewById(R.id.btnAddNoteViewNotepad);

        llNotepad = (LinearLayout) v.findViewById(R.id.llNotepad);
        noteTextToSave = (EditText) v.findViewById(R.id.etNoteText);
        noteDiscardBtn = (Button) v.findViewById(R.id.btnNoteDiscard);
        noteSaveBtn = (Button) v.findViewById(R.id.btnNoteSave);

        CompanyName.setText("Add note for " + MainActivity.ALL_COMPANIES.get(CompanyChosen.getRelatedCompanyID()).getName());
        setImageUrl(CompanyImg, MainActivity.ALL_COMPANIES.get(CompanyChosen.getRelatedCompanyID()).getImgURL());

        mtabwidget = (TabLayout) v.findViewById(R.id.tlNoteTabs);
        mtabwidget.addTab(mtabwidget.newTab().setCustomView(R.layout.custom_app_notes));
        mtabwidget.addTab(mtabwidget.newTab().setCustomView(R.layout.custom_app_notes));
        mtabwidget.addTab(mtabwidget.newTab().setCustomView(R.layout.custom_app_notes));
        mtabwidget.addTab(mtabwidget.newTab().setCustomView(R.layout.custom_app_notes));
        mtabwidget.addTab(mtabwidget.newTab().setCustomView(R.layout.custom_app_notes));

        for(int i = 0; i < mtabwidget.getTabCount(); i++)
        {
            ((ImageView)mtabwidget.getTabAt(i).getCustomView().findViewById(R.id.ivbadge_img_note)).setImageResource(CategoryImgs[i]);
            if(((TextView)mtabwidget.getTabAt(i).getCustomView().findViewById(R.id.notif_count_note)).getText().toString().equals("0"))
            {
                mtabwidget.getTabAt(i).getCustomView().findViewById(R.id.notif_count_note).setVisibility(View.GONE);
            }
        }

        mtabwidget.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                noteSelectedCategory(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                noteSelectedCategory(tab.getPosition());
            }
        });

        noteDiscardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardNote();
            }
        });
        noteSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNote();
            }
        });
        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNotesListings.setVisibility(View.GONE);
                llNotepad.setVisibility(View.VISIBLE);
            }
        });
    }

    private void noteSelectedCategory(int indexNumber)
    {
        editingNote = false;
        selectedNoteID = null;
        llNotesListings.setVisibility(View.VISIBLE);
        llNotepad.setVisibility(View.GONE);
        ArrayList<NoteModel> allnotes = new ArrayList<>();
        for (NoteModel singleNote : AllNotes.values()) {
            if(singleNote.getCategoryID() == indexNumber) allnotes.add(singleNote);
        }
        Collections.sort(allnotes, new Comparator<NoteModel>() {
            @Override
            public int compare(NoteModel o1, NoteModel o2) {
                return Double.compare(o2.getNoteID().doubleValue(), o1.getNoteID().doubleValue());
            }
        });
        NoteAdapter adapter = new NoteAdapter(getActivity(), indexNumber, allnotes, this);
        lvNotesLists.setAdapter(adapter);
    }

    private void discardNote()
    {
        if(editingNote){
            editingNote = false;
            AllNotes.remove(selectedNoteID);
            Gson gson = new Gson();
            String notesToGson = gson.toJson(AllNotes);
            try {
                FileOutputStream fOut = getContext().openFileOutput("allnotes", 0);
                fOut.write(notesToGson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            returnToViewNote();
            displayNotesNotifications();
            Toast.makeText(getContext(), "Note deleted", Toast.LENGTH_SHORT).show();
        }else
        returnToViewNote();
    }

    private void saveNote()
    {
        if(!editingNote) {
            int uniqueID = (int) System.currentTimeMillis();
            NoteModel newNote = new NoteModel(uniqueID, CompanyChosen.getRelatedCompanyID(), mtabwidget.getSelectedTabPosition(), noteTextToSave.getText().toString(), getDate());
            AllNotes.put(uniqueID, newNote);
            Gson gson = new Gson();
            String notesToGson = gson.toJson(AllNotes);
            try {
                FileOutputStream fOut = getContext().openFileOutput("allnotes", 0);
                fOut.write(notesToGson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            returnToViewNote();
            displayNotesNotifications();
            Toast.makeText(getContext(), "Note saved", Toast.LENGTH_SHORT).show();
            addCompanyToFav();
        }
        else
        {
            Toast.makeText(getContext(), "Note edited", Toast.LENGTH_SHORT).show();
            NoteModel newNote = AllNotes.get(selectedNoteID);
            editingNote = false;
            int uniqueID = (int) System.currentTimeMillis();
            NoteModel newNoteModel = new NoteModel(uniqueID, newNote.getCompanyID(), newNote.getCategoryID(), noteTextToSave.getText().toString(), getDate());
            AllNotes.put(uniqueID, newNoteModel);
            AllNotes.remove(selectedNoteID);
            Gson gson = new Gson();
            String notesToGson = gson.toJson(AllNotes);
            try {
                FileOutputStream fOut = getContext().openFileOutput("allnotes", 0);
                fOut.write(notesToGson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            returnToViewNote();
            displayNotesNotifications();
        }
    }

    private void returnToViewNote()
    {
        noteTextToSave.setText("");
        noteSelectedCategory(mtabwidget.getSelectedTabPosition());
    }

    private void displayNotesNotifications()
    {
        int[] i = new int[]{0, 0, 0, 0, 0};
        for(NoteModel note : AllNotes.values())
        {
            if(note.getCategoryID() == 0) i[0]++;
            else if(note.getCategoryID() == 1) i[1]++;
            else if(note.getCategoryID() == 2) i[2]++;
            else if(note.getCategoryID() == 3) i[3]++;
            else if(note.getCategoryID() == 4) i[4]++;
        }

        for(int j = 0; j < mtabwidget.getTabCount(); j++)
        {
            if(i[j] >= 1) {
                mtabwidget.getTabAt(j).getCustomView().findViewById(R.id.notif_count_note).setVisibility(View.VISIBLE);
                ((TextView)mtabwidget.getTabAt(j).getCustomView().findViewById(R.id.notif_count_note)).setText(String.valueOf(i[j]));
            }else mtabwidget.getTabAt(j).getCustomView().findViewById(R.id.notif_count_note).setVisibility(View.GONE);
        }

    }

    private void addCompanyToFav()
    {
        if(!MainActivity.ALL_COMPANIES_FAV.containsKey(CompanyChosen.getRelatedCompanyID()))
        {
            /*
            favourites newFav = new favourites(AllDevicesControllers.get(0).getCompany().getID(), AllDevicesControllers.get(0).getCompany(),
                    MainActivity.All_COMPANIES_INFO.get(AllDevicesControllers.get(0).getCompany().getID()));

             */
            favourites newFav = new favourites(CompanyChosen.getRelatedCompanyID(), MainActivity.ALL_COMPANIES.get(CompanyChosen.getRelatedCompanyID()),
                    MainActivity.All_COMPANIES_INFO.get(CompanyChosen.getRelatedCompanyID()));
            MainActivity.ALL_COMPANIES_FAV.put(CompanyChosen.getRelatedCompanyID(), newFav);
            Gson gson = new Gson();
            String favCompanyJson = gson.toJson(MainActivity.ALL_COMPANIES_FAV);
            try {
                FileOutputStream fOut = getContext().getApplicationContext().openFileOutput("favcompanies", 0);
                fOut.write(favCompanyJson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            NoteActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
            MainActivity.mfavBtnCount = NoteActivity.mfavBtnCount;
            MainActivity.favBtn.setText(String.valueOf(NoteActivity.mfavBtnCount));
            NoteActivity.favBtn.setText(String.valueOf(NoteActivity.mfavBtnCount));
            if(NoteActivity.favBtn.getVisibility() == View.GONE
                    && MainActivity.ALL_COMPANIES_FAV.size() >= 1)
                NoteActivity.favBtn.setVisibility(View.VISIBLE);
                MainActivity.favBtn.setVisibility(View.VISIBLE);
        }
    }

    private void setImageUrl(NetworkImageView givenImage, String givenUrl)
    {
        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }

    public String getDate()
    {
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, hh:mm a");
        String date = df.format(Calendar.getInstance().getTime());
        return date;
    }

    public void editNote(Integer NoteID) {
        editingNote = true;
        selectedNoteID = NoteID;
        llNotesListings.setVisibility(View.GONE);
        llNotepad.setVisibility(View.VISIBLE);
        noteTextToSave.setText(AllNotes.get(NoteID).getSavedText());
    }

    @Override
    public void deleteNote(Integer NoteID) {
        selectedNoteID = NoteID;
        AllNotes.remove(selectedNoteID);
        Gson gson = new Gson();
        String notesToGson = gson.toJson(AllNotes);
        try {
            FileOutputStream fOut = getContext().openFileOutput("allnotes", 0);
            fOut.write(notesToGson.getBytes());
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        returnToViewNote();
        displayNotesNotifications();
        Toast.makeText(getContext(), "Note deleted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void viewNote(Integer NoteID) {
        selectedNoteID = null;
        AlertDialog.Builder noteView = new AlertDialog.Builder(getContext());
        noteView.setMessage(AllNotes.get(NoteID).getSavedText());
        noteView.setCancelable(true);
        noteView.show();
    }

    public class loadData extends AsyncTask<Void, Integer, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... params) {
            loadNotes();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            noteSelectedCategory(mtabwidget.getSelectedTabPosition());
        }

        private void loadNotes()
        {
            try {
                FileInputStream fIn = getContext().openFileInput("allnotes");
                int c;
                String temp = "";
                while ((c = fIn.read()) != -1) {
                    temp = temp + Character.toString((char) c);
                }

                fIn.close();
                Gson gson = new Gson();
                TypeToken<HashMap<Integer, NoteModel>> list = new TypeToken<HashMap<Integer, NoteModel>>(){};
                AllNotes = gson.fromJson(temp, list.getType());
                displayNotesNotifications();
            }
            catch (Exception e){}
        }
    }
}
