package com.aiiasolution.ies.conventionscanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import com.aiiasolution.ies.conventionscanner.NoteActivity;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.example.ies.conventionscanner.R;
import com.aiiasolution.ies.conventionscanner.SharedContactsActivity;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IES on 22/07/2016.
 */
public class favouritesAdapter extends ArrayAdapter<favourites> {


    private ArrayList<NoteModel> AllNotes = new ArrayList<>();
    private ImageLoader mImageLoader;

    public favouritesAdapter(Context context, int resource, ArrayList<favourites> objects, ArrayList<NoteModel> AllNotes) {
        super(context, resource, objects);
        this.AllNotes = AllNotes;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fav_list_custom, parent, false);
        }

        NetworkImageView nivFavImage = (NetworkImageView) convertView.findViewById(R.id.nvfavImagell);
        ProgressBar pbFavProgress = (ProgressBar) convertView.findViewById(R.id.pbFavProgress);
        TextView tvFavName = (TextView) convertView.findViewById(R.id.tvfavNamell);

        LinearLayout shareContact = (LinearLayout) convertView.findViewById(R.id.icfavActions).findViewById(R.id.llsharecontact);
        LinearLayout addMeeting = (LinearLayout) convertView.findViewById(R.id.icfavActions).findViewById(R.id.lladdmeeting);
        LinearLayout viewNote = (LinearLayout) convertView.findViewById(R.id.icfavActions).findViewById(R.id.lltakenote);
        LinearLayout viewBrochure = (LinearLayout) convertView.findViewById(R.id.icfavActions).findViewById(R.id.lladdbrochure);

        TextView favNotes = (TextView) convertView.findViewById(R.id.icfavActions).findViewById(R.id.tvFavNoteNotify);
        int i = 0;
        for(int j = 0; j < AllNotes.size(); j++) {
            if(AllNotes.get(j).getCompanyID().equals(getItem(position).getFavCompanyID()))i++;
        }
        if(i >= 1) {
            favNotes.setVisibility(View.VISIBLE);
            favNotes.setText(String.valueOf(i));
        }

        pbFavProgress.getIndeterminateDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
        setImageUrl(nivFavImage, getItem(position).getFavCompany().getImgURL(), pbFavProgress);

        tvFavName.setText(getItem(position).getFavCompany().getName());



        shareContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
                v.startAnimation(blinkAnim);
                Intent openShared = new Intent(getContext(), SharedContactsActivity.class);
                openShared.putExtra("companyID", getItem(position).getFavCompanyID());
                getContext().startActivity(openShared);
            }
        });

        addMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
                v.startAnimation(blinkAnim);
                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.Events.TITLE, getItem(position).getFavCompany().getName());
                getContext().startActivity(intent);
            }
        });

        viewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
                v.startAnimation(blinkAnim);
                Intent intent = new Intent(getContext(), NoteActivity.class);
                intent.putExtra("companyID", getItem(position).getFavCompanyID());
                getContext().startActivity(intent);
            }
        });

        viewBrochure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation blinkAnim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
                v.startAnimation(blinkAnim);
                Toast.makeText(getContext(), getItem(position).getFavCompany().getName() + " brochure has been added inside your account", Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }

    private void setImageUrl(final NetworkImageView givenImage, String givenUrl, final ProgressBar pbImage)
    {
        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        mImageLoader.get(givenUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap() != null) {
                    pbImage.setVisibility(View.GONE);
                    givenImage.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }


}
