package com.aiiasolution.ies.conventionscanner;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aiiasolution.ies.conventionscanner.Fragments.NoteFragment;
import com.aiiasolution.ies.conventionscanner.Fragments.favoritesFragment;
import com.example.ies.conventionscanner.R;

public class NoteActivity extends AppCompatActivity {


    public static TextView favBtn;
    public static Boolean vViewFavorites = false;
    public static Integer mfavBtnCount = 0;

    private NoteFragment mMainFrag;
    private Fragment mFragment;
    private Integer mFragmentIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initValues();
        initViews();
        showFragment(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        MenuItem count = menu.findItem(R.id.action_favorites);
        MenuItemCompat.setActionView(count, R.layout.badge_layout);
        View view = MenuItemCompat.getActionView(count);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!vViewFavorites)
                    viewFavourites();
            }
        });
        favBtn = (TextView) view.findViewById(R.id.notif_count_badge);
        favBtn.setVisibility(View.GONE);
        if(mfavBtnCount >= 1)
        {
            favBtn.setVisibility(View.VISIBLE);
            favBtn.setText(String.valueOf(mfavBtnCount));
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(mFragmentIndex == 1) {
            finish();
        }else
        if(mFragmentIndex == 2) {
            mFragmentIndex = 1;
            vViewFavorites = false;
            super.onBackPressed();
        } else
            super.onBackPressed();
    }

    private void initValues()
    {
        mfavBtnCount = MainActivity.mfavBtnCount;
    }

    private void initViews()
    {
        getSupportActionBar().setTitle("");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void showFragment(int fragmentNumber)
    {
        mFragmentIndex = fragmentNumber;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(fragmentNumber == 1) {
            mFragment = new NoteFragment();
            mMainFrag = (NoteFragment)mFragment;
        }
        if(fragmentNumber == 2) {
            mFragment = new favoritesFragment();
        }
        fragmentTransaction.replace(R.id.gridViewFragsNote, mFragment).addToBackStack(null).commit();
    }

    private void viewFavourites()
    {
        vViewFavorites = true; showFragment(2);
    }


}
