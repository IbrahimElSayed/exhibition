package com.aiiasolution.ies.conventionscanner.Helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by IES on 28/07/2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "";
    public static final String COLUMN_ID = "ID";
    public static final ArrayList<String> COLUMNS = new ArrayList<>();

    private static final String DATABASE_NAME = "myDB.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table " + TABLE_NAME + "( " + COLUMN_ID +
            " integer primary key autoincrement, " + COLUMNS + " text not null);";


    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

}