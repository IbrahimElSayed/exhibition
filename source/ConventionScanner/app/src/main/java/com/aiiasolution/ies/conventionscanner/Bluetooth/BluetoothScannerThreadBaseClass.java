package com.aiiasolution.ies.conventionscanner.Bluetooth;
/**
 * Created by Samah SHAYYA on 03/04/2016.
 * Used to enable implementation based on SdkVersion.
 */

import java.util.List;


public abstract class BluetoothScannerThreadBaseClass extends Thread {

    public boolean endFlag = false;
    //to be overridden
    @Override
    public void start(){
        //do nothing
    }

    //to be overridden
    public abstract void terminate();

    public static int updatingPeriodMilliSeconds = 5000;

    public List<BluetoothDeviceRecord> bluetoothDeviceRecordArrayList;
    public interface BlueToothListener{
        void ShowBluetoothData(List<BluetoothDeviceRecord> Data, Boolean FullData);
    }

    public void SetBluetoothScanListener(BlueToothListener BT_Listener){mBluetoothListener = BT_Listener;}
    public BlueToothListener mBluetoothListener;
}
