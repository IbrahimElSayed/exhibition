package com.aiiasolution.ies.conventionscanner.Bluetooth;
/**
 * Created by Samah SHAYYA on 03/04/2016.
 * Edited by Ibrahim El Sayed
 * Intended for Sdk version greater than or equal 21.
 */

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.GZIPOutputStream;

//requires minSdkVersion 21
@SuppressLint("NewApi")
public class BluetoothScannerThreadSdkVersionMin21 extends BluetoothScannerThreadBaseClass {

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build();
    private List<ScanFilter> filters = new ArrayList<>();
    private List<BluetoothDeviceRecord> averageScanned = new ArrayList<>();
    private boolean updateTime = false;
    private Context mContext;

    private long registryTimeStamp = 0; //milliseconds

    private String trajectoryID;
    private boolean registerLocalBluetoothDeviceInfo = true;

    private Handler handler;

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            if(endFlag){return;}//do nothing

            if (result != null && MainActivity.FILTER_ADDRESS.contains(result.getDevice().getAddress().toLowerCase()))
            {
                if (updateTime)
                {
                    updateTime = false;
                    registryTimeStamp = System.currentTimeMillis();
                    //provideAverageRSSI(averageScanned);
                    //bluetoothDeviceRecordArrayList = averageScanned;

                    if(MainActivity.TESTING_CSV) {
                        ArrayList<BluetoothDeviceRecord> temporaryList = new ArrayList<>();
                        temporaryList.addAll(bluetoothDeviceRecordArrayList);

                        try {
                            generateBluetoothInfoAsCSV(temporaryList);
                            temporaryList.clear();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    ArrayList<BluetoothDeviceRecord> tempArray = new ArrayList<>();
                    tempArray.addAll(bluetoothDeviceRecordArrayList);
                    Collections.sort(tempArray, new Comparator<BluetoothDeviceRecord>() {
                        @Override
                        public int compare(BluetoothDeviceRecord o1, BluetoothDeviceRecord o2) {
                            return (o1.getRSSI() < o2.getRSSI() ? -1 : (o1.getRSSI() == o2.getRSSI() ? 0 : 1));
                        }
                    });
                    tempArray = removeDuplicatesFilter(tempArray);

                    mBluetoothListener.ShowBluetoothData(tempArray, false);
                    bluetoothDeviceRecordArrayList.clear();
                    //averageScanned.clear();
                }

                //getting device information:
                String deviceName = result.getDevice().getName();
                String deviceAddress = result.getDevice().getAddress();
                String deviceType;

                switch (result.getDevice().getType())
                {
                    case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                        deviceType = "Classic";
                        break;
                    case BluetoothDevice.DEVICE_TYPE_DUAL:
                        deviceType = "Dual";
                        break;
                    case BluetoothDevice.DEVICE_TYPE_LE:
                        deviceType = "LE";
                        break;
                    default://meaning BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                        deviceType = "Unknown";
                }

                int RSSI = result.getRssi();
                BluetoothDeviceRecord itemToAdd;

                if(result.getScanRecord()!= null)
                {
                    String manufacturer = result.getScanRecord().getManufacturerSpecificData().toString();
                    int advertiseFlags = result.getScanRecord().getAdvertiseFlags(); // -1: not set
                    String serviceData ;
                    if(result.getScanRecord().getServiceData()!= null)
                    {
                        serviceData = result.getScanRecord().getServiceData().toString();
                    } else {serviceData = "";}
                    String uuids;
                    if(result.getScanRecord().getServiceUuids() != null)
                    {
                        uuids = result.getScanRecord().getServiceUuids().toString();
                    } else {uuids = "";}

                    int txLevel = result.getScanRecord().getTxPowerLevel();
                    itemToAdd = new BluetoothDeviceRecord(registryTimeStamp, deviceName, deviceAddress, deviceType, RSSI, txLevel, manufacturer,
                            advertiseFlags, uuids, serviceData);

                }else{itemToAdd = new BluetoothDeviceRecord(registryTimeStamp, deviceName, deviceAddress, deviceType, RSSI);}

                bluetoothDeviceRecordArrayList.add(itemToAdd);
                //averageScanned.add(itemToAdd);
            }
        }
    };

    public BluetoothScannerThreadSdkVersionMin21(Context context, String TrajectoryID) {
        mContext = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        this.trajectoryID = TrajectoryID;
        if(bluetoothAdapter == null)
        {
            Toast.makeText(context, "Your device does not support bluetooth! Bluetooth scanner will be disabled.", Toast.LENGTH_LONG).show();
            endFlag = true;
            return;
        }

        if (!bluetoothAdapter.isEnabled())
        {
            Toast.makeText(context, "Bluetooth is disabled. It is being enabled...", Toast.LENGTH_SHORT).show();
            bluetoothAdapter.enable();
        }

        bluetoothDeviceRecordArrayList = Collections.synchronizedList(new ArrayList<BluetoothDeviceRecord>());
    }

    @Override
    public void start() {
        scheduledBluetoothScan();
    }

    //use this before sending email
    @Override
    public void terminate()
    {
        endFlag = true;

        if(bluetoothLeScanner != null)
        {
            handler.removeCallbacksAndMessages(null);
            bluetoothLeScanner.flushPendingScanResults(scanCallback);
            bluetoothLeScanner.stopScan(scanCallback);
            bluetoothDeviceRecordArrayList.clear();
        }
    }

    //scheduled run every some period
    private void scheduledBluetoothScan() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!endFlag)
                {
                    if (bluetoothLeScanner != null) {
                        bluetoothLeScanner.flushPendingScanResults(scanCallback);
                    }
                    updateTime = true;
                    if(bluetoothAdapter != null){
                        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
                        bluetoothLeScanner.startScan(filters, settings, scanCallback);
                    }else{ tryGetBluetooth(); }
                } else
                {
                    //if endFlag: true
                    if(bluetoothLeScanner != null) {
                        bluetoothLeScanner.flushPendingScanResults(scanCallback);
                        bluetoothLeScanner.stopScan(scanCallback);
                    }
                }
                handler.postDelayed(this, updatingPeriodMilliSeconds);
            }
        }, updatingPeriodMilliSeconds);
    }

    private void tryGetBluetooth()
    {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled() && bluetoothAdapter != null) {
            Toast.makeText(mContext, "trying to enable bluetooth to scan", Toast.LENGTH_SHORT).show();
            bluetoothAdapter.enable();
        }
    }
    private void generateBluetoothInfoAsCSV(List<BluetoothDeviceRecord> bluetoothDeviceRecordArrayList)
            throws IOException {

        waitForSomeTime(1); //to assure at least one milli second difference

        final String fileName = "Bluetooth__" + trajectoryID + "__"
                + System.currentTimeMillis() + "__ALL.csv.gzip";

        //creating directory
        File smartConnectDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.
                DIRECTORY_DOCUMENTS).getAbsolutePath()
                + "/smartConnect/");

        if (!smartConnectDirectory.exists()) {
            if(!smartConnectDirectory.mkdirs()){
                throw new IOException("Could not create smartConnect directory!");
            }
        }

        File bluetoothFile = new File(smartConnectDirectory + "/" + fileName);

        OutputStream bluetoothFileOutputStream;
        GZIPOutputStream gzipBluetoothFileOutputStream;

        String Header = "";

        // generate the header
        String[] cellTitles = new String[]{"Trajectory_ID", "Time (milliseconds - ms)",
                "Device Name", "Device Address", "Type", "RSSI", "TxLevel", "Manufacturer",
                "Advertise Flags", "Uuids", "Service Data"};

        //note: number of steps really detected is temporary for now
        for (int i = 0; i < cellTitles.length; i++) {
            Header = Header + cellTitles[i];
            if (i != cellTitles.length - 1) {
                Header = Header + ",";
            }
        }

        bluetoothFileOutputStream = new FileOutputStream(bluetoothFile);
        gzipBluetoothFileOutputStream = new GZIPOutputStream(bluetoothFileOutputStream);

        gzipBluetoothFileOutputStream.write(Header.getBytes());
        gzipBluetoothFileOutputStream.write("\n".getBytes());
        gzipBluetoothFileOutputStream.flush();

        if(registerLocalBluetoothDeviceInfo){
            String localBluetoothDeviceInfo = trajectoryID + ",";
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + 0 + ",";
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + bluetoothAdapter.getName() + ",";
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + bluetoothAdapter.getAddress() + ",";
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //Type
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //RSSI
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //TxLevel
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //Manufacturer
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //Advertise Flags
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //Uuids
            localBluetoothDeviceInfo = localBluetoothDeviceInfo + ","; //Service Data

            gzipBluetoothFileOutputStream.write(localBluetoothDeviceInfo.getBytes());
            gzipBluetoothFileOutputStream.write("\n".getBytes());
            gzipBluetoothFileOutputStream.flush();
            registerLocalBluetoothDeviceInfo = false;
        }

        //removeDuplicates(bluetoothDeviceRecordArrayList);//cleaning from duplicates:
        // issue with lescan on some devices

        if (!bluetoothDeviceRecordArrayList.isEmpty()) {
            String rowData;
            for (BluetoothDeviceRecord item : bluetoothDeviceRecordArrayList) {
                rowData = trajectoryID + ",";
                rowData = rowData + item.toCSVRow(); //note: you need to add "\n"

                gzipBluetoothFileOutputStream.write(rowData.getBytes());
                gzipBluetoothFileOutputStream.write("\n".getBytes());
            }
        }

        gzipBluetoothFileOutputStream.flush();
        gzipBluetoothFileOutputStream.close();
    }

    private void waitForSomeTime(long waitTimeMilliSeconds){
        long startTime = System.currentTimeMillis();

        //noinspection StatementWithEmptyBody
        while(System.currentTimeMillis() - startTime < waitTimeMilliSeconds ){
            //do nothing
        }
    }

    private ArrayList<BluetoothDeviceRecord> removeDuplicatesFilter(ArrayList<BluetoothDeviceRecord> bluetoothArrayList)
    {
        ArrayList<BluetoothDeviceRecord> TempbluetoothArrayList = new ArrayList<>();
        ArrayList<String> AllDevicesAdress = new ArrayList<>();
        for(int i = bluetoothArrayList.size()-1; i >= 0; i--)
        {
            if(!AllDevicesAdress.contains(bluetoothArrayList.get(i).getAddress()))
            AllDevicesAdress.add(bluetoothArrayList.get(i).getAddress());
        }
        for(int i = bluetoothArrayList.size()-1; i >= 0; i--)
        {
            for(int j=0; j < AllDevicesAdress.size(); j++)
            {
                if(AllDevicesAdress.get(j).equals(bluetoothArrayList.get(i).getAddress()))
                {
                    TempbluetoothArrayList.add(bluetoothArrayList.get(i));
                    AllDevicesAdress.remove(j);
                    break;
                }
            }
        }
        return  TempbluetoothArrayList;
    }

    private void provideAverageRSSI(List<BluetoothDeviceRecord> bluetoothArrayList)
    {
        if(bluetoothArrayList.size() > 3)
        {

            ArrayList<String> alladdresses = new ArrayList<>();

            Integer[] averageDistance = new Integer[alladdresses.size()];
            Integer[] averageNum = new Integer[alladdresses.size()];
            Integer[] averageRSSI = new Integer[alladdresses.size()];

            for(int i = 0; i < alladdresses.size(); i++)
            {
                averageDistance[i] = 0;
                averageNum[i] = 0;
                averageRSSI[i] = 0;
            }

            for(BluetoothDeviceRecord data : bluetoothArrayList)
            {
                for(int i = 0; i < alladdresses.size(); i++)
                {
                    if(data.getAddress().toLowerCase().equals(alladdresses.get(i)))
                    {
                        averageDistance[i] += data.getRSSI();
                        averageNum[i]++;
                    }
                }
            }

            for(int i = 0; i < alladdresses.size(); i++)
            {
                if(averageNum[i] != 0)
                averageRSSI[i] = averageDistance[i] / averageNum[i];
            }

            for(int j = 0; j < averageScanned.size(); j++)
            {
                for(int i = 0; i < alladdresses.size(); i++)
                {
                    if(averageScanned.get(j).getAddress().toLowerCase().equals(alladdresses.get(i)))
                    {
                        BluetoothDeviceRecord b = averageScanned.get(j);
                        b.setRSSI(averageRSSI[i]);
                        averageScanned.set(j, b);
                    }
                }
            }

            Log.e("Test -- ", "Average provided");

        }
    }
}
