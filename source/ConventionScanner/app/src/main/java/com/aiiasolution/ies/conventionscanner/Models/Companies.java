package com.aiiasolution.ies.conventionscanner.Models;

import java.util.ArrayList;

/**
 * Created by IES on 19/07/2016.
 */
public class Companies {


    private ArrayList<Beacon> RelatedBeacons = new ArrayList<>();
    private Integer ID;
    private String Name;
    private String ImgURL;

    public Companies(Integer ID, String Name, String ImgURL, ArrayList<Beacon> RelatedBeacons)
    {
        this.ID = ID;
        this.Name = Name;
        this.ImgURL = ImgURL;
        this.RelatedBeacons = RelatedBeacons;
    }

    public String getImgURL() {
        return ImgURL;
    }

    public Integer getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public ArrayList<Beacon> getRelatedBeacons() {
        return RelatedBeacons;
    }

}
