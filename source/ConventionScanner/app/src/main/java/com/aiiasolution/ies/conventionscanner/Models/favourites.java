package com.aiiasolution.ies.conventionscanner.Models;

/**
 * Created by IES on 22/07/2016.
 */
public class favourites {

    private Integer favCompanyID;
    private Companies favCompany;
    private CompaniesInformation favCompanyInfo;

    public favourites(Integer favCompanyID, Companies favCompany, CompaniesInformation favCompanyInfo)
    {
        this.favCompanyID = favCompanyID;
        this.favCompany = favCompany;
        this.favCompanyInfo = favCompanyInfo;
    }

    public Integer getFavCompanyID() {
        return favCompanyID;
    }

    public Companies getFavCompany() {
        return favCompany;
    }

    public CompaniesInformation getFavCompanyInfo() {
        return favCompanyInfo;
    }


}
