package com.aiiasolution.ies.conventionscanner.Helpers;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IES on 08/07/2016.
 */
public class VolleyHelper {

    public interface VolleyHelperInterface
    {void onRecieveData(String DataVH);}
    public void setVolleyHelperListener(VolleyHelperInterface eventListener) {this.mListener=eventListener;}

    private VolleyHelperInterface mListener;
    private Context getContext;

    public VolleyHelper(Context MainContext)
    {
        this.getContext = MainContext;
    }


    //REQUEST Data using Volley, Takes URL of Web Service
    public void RequestData(String URL)
    {
        final RequestQueue queue = Volley.newRequestQueue(getContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try
                        {
                            mListener.onRecieveData(response);
                        }
                        catch (Exception e)
                        {
                            Log.w("VolleyHelper: ", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.w("VolleyHelper: ", error.getMessage());
            }
        });
        queue.add(stringRequest);
    }


    //POST Data using Volley, Takes URL and HashMap Key, Value
    public void SendData(String URL, final HashMap<String, String> Params)
    {
        final RequestQueue queue = Volley.newRequestQueue(getContext);
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        mListener.onRecieveData(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("VolleyHelper", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String>  params = Params;
                return params;
            }
        };
        queue.add(postRequest);
    }

}
