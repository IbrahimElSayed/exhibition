package com.aiiasolution.ies.conventionscanner.Models;


import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by IES on 19/07/2016.
 */
public class Rooms {

    private TextView RoomText;
    private NetworkImageView RoomImage;
    private ProgressBar RoomImageProgress;
    private FrameLayout RoomMainNotifier;

    public Rooms(TextView RoomText, NetworkImageView RoomImage, ProgressBar RoomImageProgress, FrameLayout RoomMainNotifier)
    {
        this.RoomText = RoomText;
        this.RoomImage = RoomImage;
        this.RoomImageProgress = RoomImageProgress;
        this.RoomMainNotifier = RoomMainNotifier;
    }

    public TextView getRoomText() {
        return RoomText;
    }

    public NetworkImageView getRoomImage() {
        return RoomImage;
    }

    public ProgressBar getRoomImageProgress() {
        return RoomImageProgress;
    }

    public FrameLayout getRoomMainNotifier() {
        return RoomMainNotifier;
    }


}
