package com.aiiasolution.ies.conventionscanner.Models;

/*
 * Created by IES on 27/07/2016.
 */

public class NoteModel {

    private Integer noteID;
    private Integer companyID;
    private Integer categoryID;
    private String savedText;
    private String noteDate;

    public NoteModel(Integer noteID, Integer companyID, Integer categoryID, String savedText, String noteDate)
    {
        this.noteID = noteID;
        this.companyID = companyID;
        this.categoryID = categoryID;
        this.savedText = savedText;
        this.noteDate = noteDate;
    }

    public Integer getNoteID() {
        return noteID;
    }

    public Integer getCompanyID() {
        return companyID;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public String getSavedText() {
        return savedText;
    }

    public String getNoteDate() {
        return noteDate;
    }

}
