package com.aiiasolution.ies.conventionscanner.Bluetooth;

/**
 * Created by Samah SHAYYA on 2/22/2016.
 */

public class BluetoothDeviceRecord {

    private long timeStamp; //milliseconds
    private String name;
    private String address;
    private String type; //classic, dual, le, unknown

    public void setRSSI(int RSSI) {
        this.RSSI = RSSI;
    }

    private int RSSI; //dismissed if connectionStatus = 0

    private boolean addOthers; //to indicate whether fields below are acquired or not: used for printing
    private int txLevel;
    private String manufacturer;
    private int advertiseFlags;
    private String uuids;
    private String serviceData;


    public BluetoothDeviceRecord(long timeStamp, String name, String address, String type, int RSSI){
        this.timeStamp = timeStamp;
        this.name = name;
        this.address = address;
        this.type = type;
        this.RSSI = RSSI;

        addOthers = false;  //below will be left blank in toString or toCSVRow

        txLevel = 0;
        manufacturer = "";
        advertiseFlags = -1;
        uuids = "";
        serviceData = "";
    }

    public BluetoothDeviceRecord(long timeStamp, String name, String address, String type,
                                 int RSSI, int txLevel, String manufacturer, int advertiseFlags,
                                 String uuids, String serviceData ){
        this.timeStamp = timeStamp;
        this.name = name;
        this.address = address;
        this.type = type;
        this.RSSI = RSSI;

        addOthers = true; //consider those below in toString and toCSVRow

        this.txLevel =txLevel;
        this.manufacturer = manufacturer;
        this.advertiseFlags = advertiseFlags;
        this.uuids = uuids;
        this.serviceData = serviceData;
    }

    public long getTimeStamp(){
        return  timeStamp;
    }

    public String getName(){
        return  name;
    }

    public String getAddress(){
        return address;
    }

    public String getType(){
        return type;
    }

    public int getRSSI(){
        return  RSSI;
    }

    public int getTxLevel(){
        return txLevel;
    }

    public String getManufacturer(){
        return manufacturer;
    }

    public int getAdvertiseFlags(){
        return advertiseFlags;
    }

    public String getUuids(){
        return uuids;
    }

    public String getServiceData(){
        return serviceData;
    }

    public String toString(){
        String stringValue;

        stringValue = "Time (milliseconds - ms) : " + getTimeStamp() + "\n";
        stringValue = stringValue + ", Name : " + getName() + "\n";
        stringValue = stringValue + ", Address : " + getAddress() + "\n";
        stringValue = stringValue + ", Type : " + getType() + "\n";
        stringValue = stringValue + ", RSSI : " + getRSSI() + "\n";

        if(addOthers) {
            stringValue = stringValue + ", TxLevel : " + getTxLevel() + "\n";
            stringValue = stringValue + ", Manufacturer : " + getManufacturer() + "\n";
            stringValue = stringValue + ", Advertise Flags : " + getAdvertiseFlags() + "\n";
            stringValue = stringValue + ", Uuids : " + getUuids() + "\n";
            stringValue = stringValue + ", Service Data : " + getServiceData() + "\n";
        } else {
            // add no value: keep it empty
            stringValue = stringValue + ", TxLevel : " + "\n";
            stringValue = stringValue + ", Manufacturer : " + "\n";
            stringValue = stringValue + ", Advertise Flags : " + "\n";
            stringValue = stringValue + ", Uuids : " + "\n";
            stringValue = stringValue + ", Service Data : " + "\n";
        }

        return stringValue;
    }

    public String toCSVRow(){
        String rowData;

        rowData = getTimeStamp() + ",";
        rowData = rowData + getName() + ",";
        rowData = rowData + getAddress() + ",";
        rowData = rowData + getType() + ",";
        rowData = rowData + getRSSI() + ",";

        if(addOthers) {
            rowData = rowData + getTxLevel() + ",";
            rowData = rowData + getManufacturer() + ",";
            rowData = rowData + getAdvertiseFlags() + ",";
            rowData = rowData + getUuids() + ",";
            rowData = rowData + getServiceData() + ",";
        } else {
            // add no value: keep it empty
            rowData = rowData + ",";
            rowData = rowData + ",";
            rowData = rowData + ",";
            rowData = rowData + ",";
            rowData = rowData + ",";
        }

        return rowData;
    }
}
