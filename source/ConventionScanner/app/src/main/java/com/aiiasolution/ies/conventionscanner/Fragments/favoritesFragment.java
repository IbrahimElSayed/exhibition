package com.aiiasolution.ies.conventionscanner.Fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.aiiasolution.ies.conventionscanner.Adapters.favouritesAdapter;
import com.aiiasolution.ies.conventionscanner.Interfaces.loadingCompleted;
import com.aiiasolution.ies.conventionscanner.MainActivity;
import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.example.ies.conventionscanner.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class favoritesFragment extends Fragment implements loadingCompleted.notesLoadCompleted {


    private View mainView;
    private favouritesAdapter llFavs_adapter;

    public favoritesFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(mainView == null) {
            mainView = inflater.inflate(R.layout.fragment_favorites, container, false);
            new loadData(this).execute(getContext());
        }else llFavs_adapter.notifyDataSetChanged();

        return mainView;
    }

    @Override
    public void onNotesLoaded(ArrayList<NoteModel> listNotes) {
        ListView llFavs = (ListView) mainView.findViewById(R.id.lvfavList);
        ArrayList<favourites> allfavs = new ArrayList<>();
        allfavs.addAll(MainActivity.ALL_COMPANIES_FAV.values());
        llFavs_adapter = new favouritesAdapter(getActivity(), 0, allfavs, listNotes);
        llFavs.setAdapter(llFavs_adapter);
    }


    public class loadData extends AsyncTask<Context, Integer, ArrayList<NoteModel>>
    {

        private Context context;
        private loadingCompleted.notesLoadCompleted listener;

        public loadData(loadingCompleted.notesLoadCompleted listener) {this.listener = listener; }

        @Override
        protected ArrayList<NoteModel> doInBackground(Context... params) {
            this.context = params[0];
            ArrayList<NoteModel> loadedNotes = loadNotes();
            return loadedNotes;
        }

        @Override
        protected void onPostExecute(ArrayList<NoteModel> loadedNotes) {
            listener.onNotesLoaded(loadedNotes);
        }

        private ArrayList<NoteModel> loadNotes()
        {
            try {
                FileInputStream fIn = context.openFileInput("allnotes");
                int c;
                String temp = "";
                while ((c = fIn.read()) != -1) {
                    temp = temp + Character.toString((char) c);
                }

                fIn.close();
                Gson gson = new Gson();
                TypeToken<HashMap<Integer, NoteModel>> list = new TypeToken<HashMap<Integer, NoteModel>>(){};
                NoteFragment.AllNotes = gson.fromJson(temp, list.getType());
                ArrayList<NoteModel> tempNotes = new ArrayList<>();
                tempNotes.addAll(NoteFragment.AllNotes.values());
                return  tempNotes;
            }
            catch (Exception e){
                Log.e("TEST_______", e.getMessage());
                ArrayList<NoteModel> newList = new ArrayList<>();
                newList.clear();
                return newList;
            }
        }
    }
}
