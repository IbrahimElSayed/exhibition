package com.aiiasolution.ies.conventionscanner.Interfaces;

import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import java.util.ArrayList;

/**
 * Created by IES on 29/07/2016.
 */
public interface loadingCompleted{
    interface dataLoadCompleted {
        void onDataLoaded();
    }
    interface notesLoadCompleted {
        void onNotesLoaded(ArrayList<NoteModel> listNotes);
    }
}
