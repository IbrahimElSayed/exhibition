package com.aiiasolution.ies.conventionscanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aiiasolution.ies.conventionscanner.Adapters.Contacts;
import com.aiiasolution.ies.conventionscanner.Models.CompaniesInformation;
import com.example.ies.conventionscanner.R;
import com.google.gson.Gson;

import java.io.FileOutputStream;

public class SharedContactsActivity extends AppCompatActivity {


    private ImageLoader mImageLoader;

    private CompaniesInformation retrievedInfo;

    private TabLayout tlContactsTabs;
    private NetworkImageView nvCompanyImage;
    private TextView tvCompanyName;
    private GridView gvContacts;

    private Integer[] CategoryImgs = new Integer[]{
            R.mipmap.note_team,
            R.mipmap.contacts_sync,
            R.mipmap.contacts_await,
            R.mipmap.contacts_employee
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_contacts_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initValues();
    }

    private void initViews()
    {
        getSupportActionBar().setTitle("");

        nvCompanyImage = (NetworkImageView) findViewById(R.id.nv_CS_Image);
        tvCompanyName = (TextView) findViewById(R.id.tv_CS_Name);
        gvContacts = (GridView) findViewById(R.id.gvContactsSynced);

        tlContactsTabs = (TabLayout) findViewById(R.id.tlContactsTabs);
        tlContactsTabs.addTab(tlContactsTabs.newTab().setCustomView(R.layout.custom_app_contacts));
        tlContactsTabs.addTab(tlContactsTabs.newTab().setCustomView(R.layout.custom_app_contacts));
        tlContactsTabs.addTab(tlContactsTabs.newTab().setCustomView(R.layout.custom_app_contacts));
        tlContactsTabs.addTab(tlContactsTabs.newTab().setCustomView(R.layout.custom_app_contacts));
        tlContactsTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabChanged(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        for(int i = 0; i < tlContactsTabs.getTabCount(); i++)
        {
            ((ImageView)tlContactsTabs.getTabAt(i).getCustomView().findViewById(R.id.ivbadge_img_contactTab)).setImageResource(CategoryImgs[i]);
        }

    }

    private void initValues()
    {
        retrievedInfo = MainActivity.All_COMPANIES_INFO.get(getIntent().getIntExtra("companyID", -1));
        setImageUrl(nvCompanyImage, MainActivity.ALL_COMPANIES.get(retrievedInfo.getRelatedCompanyID()).getImgURL());
        tvCompanyName.setText(MainActivity.ALL_COMPANIES.get(retrievedInfo.getRelatedCompanyID()).getName());

        Contacts adapter = new Contacts(this, retrievedInfo.getTeamMembers(), retrievedInfo.getRelatedCompanyID());
        gvContacts.setAdapter(adapter);
    }

    private void tabChanged(Integer tabNum)
    {
        //Awaiting backend
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    private void setImageUrl(NetworkImageView givenImage, String givenUrl)
    {
        mImageLoader = VolleyImageRequest.getInstance(getApplicationContext()).getImageLoader();
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == 1)
        {
            if (resultCode == RESULT_OK)
            {
                addCompanyToFav();
                Toast.makeText(getApplicationContext(), "Company added to favorites", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addCompanyToFav()
    {
        if(!MainActivity.ALL_COMPANIES_FAV.containsKey(retrievedInfo.getRelatedCompanyID()))
        {
            favourites newFav = new favourites(retrievedInfo.getRelatedCompanyID(), MainActivity.ALL_COMPANIES.get(retrievedInfo.getRelatedCompanyID()),
                    MainActivity.All_COMPANIES_INFO.get(retrievedInfo.getRelatedCompanyID()));
            MainActivity.ALL_COMPANIES_FAV.put(retrievedInfo.getRelatedCompanyID(), newFav);
            Gson gson = new Gson();
            String favCompanyJson = gson.toJson(MainActivity.ALL_COMPANIES_FAV);
            try {
                FileOutputStream fOut = getApplicationContext().openFileOutput("favcompanies", 0);
                fOut.write(favCompanyJson.getBytes());
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            MainActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
            MainActivity.favBtn.setText(String.valueOf(NoteActivity.mfavBtnCount));
            MainActivity.favBtn.setVisibility(View.VISIBLE);
        }
    }


}
