package com.aiiasolution.ies.conventionscanner.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.MainActivity;
import com.aiiasolution.ies.conventionscanner.Models.Companies;
import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.ies.conventionscanner.R;

import java.util.ArrayList;

/**
 * Created by IES on 27/07/2016.
 */
public class NoteAdapter extends ArrayAdapter<NoteModel> {

    private viewNote mViewNote;
    public interface viewNote{
        void editNote(Integer NoteID);
        void deleteNote(Integer NoteID);
        void viewNote(Integer NoteID);
    }

    private ImageLoader mImageLoader;

    public NoteAdapter(Context context, int resource, ArrayList<NoteModel> objects, viewNote mViewNote) {
        super(context, resource, objects);
        this.mViewNote = mViewNote;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_note_adapter, parent, false);
        }

        NetworkImageView companyNote = (NetworkImageView) convertView.findViewById(R.id.nvNoteViewImgNetowrk);
        TextView noteDate = (TextView) convertView.findViewById(R.id.tvNoteViewDate);
        final ImageView viewNote = (ImageView) convertView.findViewById(R.id.ivNoteViewImgBtn);
        final ImageView editNote = (ImageView) convertView.findViewById(R.id.ivNoteEditImgBtn);
        final ImageView deleteNote = (ImageView) convertView.findViewById(R.id.ivNoteDelImgBtn);

        noteDate.setText(getItem(position).getNoteDate());
        setImageUrl(companyNote, MainActivity.ALL_COMPANIES.get(getItem(position).getCompanyID()).getImgURL());

        viewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewNote.viewNote(getItem(position).getNoteID());
            }
        });

        editNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewNote.editNote(getItem(position).getNoteID());
            }
        });

        deleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewNote.deleteNote(getItem(position).getNoteID());
            }
        });

        return convertView;
    }

    private void setImageUrl(NetworkImageView givenImage, String givenUrl)
    {
        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }
}
