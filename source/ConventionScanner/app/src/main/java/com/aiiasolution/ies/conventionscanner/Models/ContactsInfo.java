package com.aiiasolution.ies.conventionscanner.Models;

/**
 * Created by IES on 21/07/2016.
 */
public class ContactsInfo {

    private Integer ContactID;
    private String ContactPic;
    private String ContactName;
    private String ContactTitle;
    private Boolean ContactSync;
    private String ContactEmail;
    private String ContactPhone;
    private String ContactPhone2;

    public ContactsInfo(Integer ContactID, String ContactPic, String ContactName, String ContactTitle, Boolean ContactSync,
                        String ContactEmail, String ContactPhone, String ContactPhone2)
    {
        this.ContactID = ContactID;
        this.ContactPic = ContactPic;
        this.ContactName = ContactName;
        this.ContactTitle = ContactTitle;
        this.ContactSync = ContactSync;
        this.ContactEmail = ContactEmail;
        this.ContactPhone = ContactPhone;
        this.ContactPhone2 = ContactPhone2;
    }

    public Integer getContactID() {
        return ContactID;
    }

    public String getContactPic() {
        return ContactPic;
    }

    public String getContactTitle() {
        return ContactTitle;
    }

    public String getContactName() {
        return ContactName;
    }

    public Boolean getContactSync() {
        return ContactSync;
    }

    public String getContactPhone() {
        return ContactPhone;
    }

    public String getContactPhone2() {
        return ContactPhone2;
    }

    public String getContactEmail() {
        return ContactEmail;
    }



}
