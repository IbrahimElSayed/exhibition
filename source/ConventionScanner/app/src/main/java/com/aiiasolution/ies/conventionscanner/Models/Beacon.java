package com.aiiasolution.ies.conventionscanner.Models;

/**
 * Created by IES on 15/07/2016.
 */
public class Beacon {

    public Integer getBeaconID() {
        return BeaconID;
    }

    public String getBeaconName() {
        return BeaconName;
    }

    private Integer BeaconID;
    private String BeaconName;

    public Beacon(int BeaconID, String BeaconName)
    {
        this.BeaconID = BeaconID;
        this.BeaconName = BeaconName;
    }

}
