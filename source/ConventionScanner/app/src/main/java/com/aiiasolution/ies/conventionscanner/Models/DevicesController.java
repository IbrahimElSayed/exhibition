package com.aiiasolution.ies.conventionscanner.Models;

/**
 * Created by IES on 13/07/2016.
 */
public class DevicesController {


    private Integer ID;
    private String Name;
    private String RRSI;
    private String EstimateRange;
    private String DeviceID;
    private String TimeStamp;
    private Companies Company;

    public DevicesController(Integer ID, String Name, String RSSI, String EstimateRange, String DeviceID, String TimeStamp,
                             Companies Company)
    {
        this.ID = ID;
        this.Name = Name;
        this.RRSI = RSSI;
        this.EstimateRange = EstimateRange;
        this.DeviceID = DeviceID;
        this.TimeStamp = TimeStamp;
        this.Company = Company;
    }

    public String toCSVRow()
    {
        String rowData = "";
        rowData = getTimeStamp() + ",";
        rowData += getDeviceID() + ",";
        rowData += getName() + ",";
        rowData += getRRSI() + ",";
        rowData += getEstimateRange() + ",";
        return rowData;
    }

    public String getName() {
        return Name;
    }

    public String getRRSI() {
        return RRSI;
    }

    public String getEstimateRange() {
        return EstimateRange;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public Integer getID() {
        return ID;
    }

    public Companies getCompany() {
        return Company;
    }

}
