package com.aiiasolution.ies.conventionscanner.Models;

import java.util.ArrayList;

/**
 * Created by IES on 19/07/2016.
 */
public class CompaniesInformation {

    private String TeamLeader;
    private ArrayList<ContactsInfo> TeamMembers = new ArrayList<>();
    private String Email;
    private String Website;
    private String Catalogue;
    private String PhoneNumber;
    private String VideoURL;
    private Boolean Liked = false;
    private Integer RelatedCompanyID;

    public CompaniesInformation(String TeamLeader, ArrayList<ContactsInfo> TeamMembers, String Email, String Webiste, String Catalogue,
                                String PhoneNumber, String VideoURL, Boolean Liked, Integer RelatedCompanyID)
    {
        this.TeamLeader = TeamLeader;
        this.TeamMembers = TeamMembers;
        this.Email = Email;
        this.Website = Webiste;
        this.Catalogue = Catalogue;
        this.PhoneNumber = PhoneNumber;
        this.VideoURL = VideoURL;
        this.Liked = Liked;
        this.RelatedCompanyID = RelatedCompanyID;
    }


    public ArrayList<ContactsInfo> getTeamMembers() {
        return TeamMembers;
    }

    public String getTeamLeader() {
        return TeamLeader;
    }

    public String getEmail() {
        return Email;
    }

    public String getWebsite() {
        return Website;
    }

    public String getCatalogue() {
        return Catalogue;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public String getVideoURL() {
        return VideoURL;
    }

    public Boolean getLiked() {
        return Liked;
    }

    public Integer getRelatedCompanyID() {
        return RelatedCompanyID;
    }

}
