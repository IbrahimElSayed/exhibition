package com.aiiasolution.ies.conventionscanner;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Interfaces.loadingCompleted;
import com.aiiasolution.ies.conventionscanner.Models.Beacon;
import com.aiiasolution.ies.conventionscanner.Models.Companies;
import com.aiiasolution.ies.conventionscanner.Models.CompaniesInformation;
import com.aiiasolution.ies.conventionscanner.Models.ContactsInfo;
import com.aiiasolution.ies.conventionscanner.Models.NoteModel;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.example.ies.conventionscanner.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class SplashScreenActivity extends AppCompatActivity implements loadingCompleted.dataLoadCompleted{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new loadData(SplashScreenActivity.this).execute();
            }
        }, 4000);
    }

    @Override
    public void onDataLoaded() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private class loadData extends AsyncTask<Void, Integer, String>
    {
        private loadingCompleted.dataLoadCompleted listener;

        public loadData(loadingCompleted.dataLoadCompleted listener) {this.listener = listener; }

        @Override
        protected String doInBackground(Void... params) {
            initData();
            return "Loading done.";
        }

        @Override
        protected void onPostExecute(String s) {
            listener.onDataLoaded();
        }

        private void initData()
        {

            loadFavourites();

            ArrayList<Beacon> AiiABeacons = new ArrayList<>();
            AiiABeacons.add(new Beacon(1, "19:18:fc:01:4b:0d"));
            AiiABeacons.add(new Beacon(1, "19:18:fc:01:4a:aa"));
            AiiABeacons.add(new Beacon(1, "19:18:fc:01:4a:ac"));

            ArrayList<Beacon> airBnbBeacons = new ArrayList<>();
            airBnbBeacons.add(new Beacon(2, "19:18:fc:01:4a:b6"));
            airBnbBeacons.add(new Beacon(2, "19:18:fc:01:4a:f6"));
            airBnbBeacons.add(new Beacon(2, "19:18:fc:01:4b:09"));

            ArrayList<Beacon> SamsungBeacons = new ArrayList<>();
            SamsungBeacons.add(new Beacon(3, "19:18:fc:01:4a:a4"));
            SamsungBeacons.add(new Beacon(3, "19:18:fc:01:4a:fd"));
            SamsungBeacons.add(new Beacon(3, "19:18:fc:01:4b:13"));

            ArrayList<Beacon> AliBabaBeacons = new ArrayList<>();
            AliBabaBeacons.add(new Beacon(4, "19:18:fc:01:4b:0f"));
            AliBabaBeacons.add(new Beacon(4, "19:18:fc:01:4a:b7"));

            ArrayList<Beacon> MicrosoftBeacons = new ArrayList<>();
            MicrosoftBeacons.add(new Beacon(5, "19:18:fc:01:4a:b5"));
            MicrosoftBeacons.add(new Beacon(6, "19:18:fc:01:4a:fc"));
            MicrosoftBeacons.add(new Beacon(5, "19:18:fc:01:4a:9f"));
            MicrosoftBeacons.add(new Beacon(6, "19:18:fc:01:4a:ff"));

            ArrayList<Beacon> AmazonBeacons = new ArrayList<>();
            AmazonBeacons.add(new Beacon(7, "19:18:fc:01:4a:b1"));
            AmazonBeacons.add(new Beacon(7, "19:18:fc:01:4b:0b"));
            AmazonBeacons.add(new Beacon(7, "19:18:fc:01:4a:9d"));

            ArrayList<Beacon> EbayBeacons = new ArrayList<>();
            EbayBeacons.add(new Beacon(8, "19:18:fc:01:4a:a3"));
            EbayBeacons.add(new Beacon(8, "19:18:fc:01:4b:0e"));

            ArrayList<Beacon> GoogleBeacons = new ArrayList<>();
            GoogleBeacons.add(new Beacon(9, "19:18:fc:01:4b:07"));
            GoogleBeacons.add(new Beacon(10, "19:18:fc:01:4b:01"));
            GoogleBeacons.add(new Beacon(9,  "19:18:fc:01:4a:9e"));
            GoogleBeacons.add(new Beacon(10, "19:18:fc:01:4a:b3"));

            ArrayList<Beacon> FacebookBeacons = new ArrayList<>();
            FacebookBeacons.add(new Beacon(11, "19:18:fc:01:4b:03"));
            FacebookBeacons.add(new Beacon(11, "19:18:fc:01:4a:fe"));

            ArrayList<Beacon> NetflixBeacons = new ArrayList<>();
            NetflixBeacons.add(new Beacon(12, "19:18:fc:01:4a:9c"));
            NetflixBeacons.add(new Beacon(12, "19:18:fc:01:4a:ae"));

            ArrayList<Beacon> AppleBeacons = new ArrayList<>();
            AppleBeacons.add(new Beacon(13, "19:18:fc:01:4a:b8"));
            AppleBeacons.add(new Beacon(14, "19:18:fc:01:4a:fb"));
            AppleBeacons.add(new Beacon(13, "19:18:fc:01:4a:fa"));
            AppleBeacons.add(new Beacon(14, "19:18:fc:01:4b:06"));
            AppleBeacons.add(new Beacon(13, "19:18:fc:01:4b:05"));
            AppleBeacons.add(new Beacon(14, "19:18:fc:01:4a:a7"));

            ArrayList<Beacon> QanawatBeacons = new ArrayList<>();
            QanawatBeacons.add(new Beacon(15, "19:18:fc:01:4b:12"));
            QanawatBeacons.add(new Beacon(15, "19:18:fc:01:4a:a8"));

            ArrayList<Beacon> TwitterBeacons = new ArrayList<>();
            TwitterBeacons.add(new Beacon(16, "19:18:fc:01:4a:a0"));
            TwitterBeacons.add(new Beacon(16, "19:18:fc:01:4a:ad"));

            ArrayList<Beacon> UberBeacons = new ArrayList<>();
            UberBeacons.add(new Beacon(17, "19:18:fc:01:4a:a2"));
            UberBeacons.add(new Beacon(17, "19:18:fc:01:4b:08"));

            ArrayList<Beacon> YahooBeacons = new ArrayList<>();
            YahooBeacons.add(new Beacon(18, "19:18:fc:01:4a:f8"));
            YahooBeacons.add(new Beacon(18, "19:18:fc:01:4b:0c"));


            MainActivity.ALL_COMPANIES.put(0, new Companies(0, "AiiA", "http://130.251.47.120/aiia/wp-content/uploads/2016/04/logo_aiia.png", AiiABeacons));
            MainActivity.ALL_COMPANIES.put(1, new Companies(1, "Microsoft", "http://cdn1.iconfinder.com/data/icons/flat-and-simple-part-1/128/microsoft-128.png", MicrosoftBeacons));
            MainActivity.ALL_COMPANIES.put(2, new Companies(2, "Google", "http://cdn4.iconfinder.com/data/icons/new-google-logo-2015/400/new-google-favicon-128.png", GoogleBeacons));
            MainActivity.ALL_COMPANIES.put(3, new Companies(3, "Apple", "http://cdn0.iconfinder.com/data/icons/glyph_set/128/apple.png", AppleBeacons));
            MainActivity.ALL_COMPANIES.put(4, new Companies(4, "YouTube", "https://upload.wikimedia.org/wikipedia/commons/7/71/New_youtube_logo.png", SamsungBeacons));
            MainActivity.ALL_COMPANIES.put(5, new Companies(5, "airBnb", "http://cdn0.iconfinder.com/data/icons/picons-social/57/68-airbnb-2-128.png", airBnbBeacons));
            MainActivity.ALL_COMPANIES.put(6, new Companies(6, "AliBaba", "http://cdn4.iconfinder.com/data/icons/flat-brand-logo-2/512/alibaba-128.png", AliBabaBeacons));
            MainActivity.ALL_COMPANIES.put(7, new Companies(7, "Amazon", "http://icons.iconarchive.com/icons/chrisbanks2/cold-fusion-hd/128/amazon-black-icon.png", AmazonBeacons));
            MainActivity.ALL_COMPANIES.put(8, new Companies(8, "Ebay", "http://cdn4.iconfinder.com/data/icons/flat-brand-logo-2/512/ebay-128.png", EbayBeacons));
            MainActivity.ALL_COMPANIES.put(9, new Companies(9, "Facebook", "http://cdn0.iconfinder.com/data/icons/yooicons_set01_socialbookmarks/256/social_facebook_box_blue.png", FacebookBeacons));
            MainActivity.ALL_COMPANIES.put(10, new Companies(10, "Netflix", "http://icons.iconarchive.com/icons/chrisbanks2/cold-fusion-hd/128/netflix-2-icon.png", NetflixBeacons));
            MainActivity.ALL_COMPANIES.put(11, new Companies(11, "Qanawat", "http://dfnp-images-ldc.decypha.net/companies/32876.jpg", QanawatBeacons));
            MainActivity.ALL_COMPANIES.put(12, new Companies(12, "Twitter", "http://cdn1.iconfinder.com/data/icons/logotypes/32/twitter-128.png", TwitterBeacons));
            MainActivity.ALL_COMPANIES.put(13, new Companies(13, "Uber", "http://cdn1.iconfinder.com/data/icons/social-shade-rounded-rects/512/uber-128.png", UberBeacons));
            MainActivity.ALL_COMPANIES.put(14, new Companies(14, "Yahoo", "http://cdn1.iconfinder.com/data/icons/black-socicons/512/yahoo-128.png", YahooBeacons));

            for(Companies SingleCompany : MainActivity.ALL_COMPANIES.values())
            {
                for(Beacon SingleBeacon : SingleCompany.getRelatedBeacons())
                {
                    MainActivity.FILTER_ADDRESS.add(SingleBeacon.getBeaconName());
                }
            }

            ArrayList<ContactsInfo> AiiAContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/79.jpg", "Marwan Hakim", "CEO", false, "marwan.hakim@aiia.com", "0079", "0079"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/80.jpg", "Wisam Hakim", "CCO", true, "wisam.hakim@aiia.com", "0080", "0080"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Samer Ghosn", "CTO", true, "samer.hakim@aiia.com", "0081", "0081"));
                add(new ContactsInfo(3, "http://www.networkautomation.com/images/urc/blog/main/4dfa24bb9760c443545782.jpg", "Developer AiiA", "Developer", true, "developer.aiia@aiia.com", "0082", "0082"));
                add(new ContactsInfo(4, "http://worldmaritimenews.com/wp-content/uploads/2014/01/BMT-Appoints-New-Business-Development-Manager.jpg", "Business Development", "Bus Dev", true, "bus.dev@aiia.com", "0083", "0083"));
                add(new ContactsInfo(5, "http://www.forsythgroup.com/wp-content/uploads/2013/07/Barrett.jpg", "Marketing Director", "Mktg Director", true, "mktg.director@aiia.com", "0084", "0084"));
            }};

            ArrayList<ContactsInfo> MicrosoftContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/13.jpg", "Bill Gates", "CEO", true, "bill.gates@microsoft.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/14.jpg", "Steve Balmer", "CFO", true, "steve.balmer@microsoft.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/15.jpg", "Satya Nadella", "CTO", true, "satya.nadella@microsoft.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Microsoft", "Developer", true, "developer.microsoft@microsoft.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "Bus Dev", true, "bus.dev@microsoft.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "Mktg Director", true, "mktg.director@microsoft.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> TestContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "CEO", true, "test@test.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "CFO", false, "test@test.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "CTO", true, "test@test.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "DEV", false, "test@test.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "BUS", true, "test@test.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "test", "MKT", false, "test@test.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> AppleContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/1.jpg", "Steve Jobs", "CEO", true, "Steve.Jobs@apple.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/2.jpg", "Ronald Wayne", "CFO", true, "Ronald.Wayne@apple.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/3.jpg", "Steve Wozniak", "CTO", false, "Steve.Wozniak@apple.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Apple", "DEV", false, "dev.apple@apple.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@apple.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@apple.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> GoogleContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/7.jpg", "Eric Schmidt", "CEO", true, "Eric.Shmidt@Google.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/8.jpg", "Larry Page", "CFO", true, "Larry.Page@Google.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/9.jpg", "Sergey Brin", "CTO", false, "Sergey.Brin@Google.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Google", "DEV", false, "dev.Google@Google.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@Google.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@Google.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> YahooContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/19.jpg", "Marissa Mayer", "CEO", true, "mariisa.mayer@yahoo.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/20.jpg", "David Filo", "CFO", true, "david.filo@yahoo.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/21.jpg", "Jerry Yang", "CTO", false, "jerry.yang@yahoo.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Yahoo", "DEV", false, "dev.yahoo@yahoo.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@yahoo.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@yahoo.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> EbayContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/25.jpg", "Pierre Omidyar", "CEO", true, "Pierre.Omidyar@ebay.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/26.jpg", "Elon Musk", "CFO", true, "Elon.Musk@ebay.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/27.jpg", "Robert Chatwany", "CTO", false, "Robert.Chatwany@ebay.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Ebay", "DEV", false, "dev.apple@ebay.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@ebay.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@ebay.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> AmazonContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/31.jpg", "Jeff Bezos", "CEO", true, "jeff.bezos@amazon.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/32.jpg", "Andrew Rassy", "CFO", true, "andrew.rassy@amazon.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/33.jpg", "Brian Olasvky", "CTO", false, "brian.olavsky@amazon.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Amazon", "DEV", false, "dev.amazon@amazon.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@amazon.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@amazon.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> YouTubeContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/37.jpg", "Chad Hurley", "CEO", true, "char.hurley@youtube.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/38.jpg", "Jawed Karim", "CFO", true, "jawed.karim@youtube.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/39.jpg", "Steve Chen", "CTO", false, "steve.chen@youtube.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer YouTube", "DEV", false, "dev.youtube@youtube.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@youtube.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@youtube.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> FacebookContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/49.jpg", "Mark Zuckerberg", "CEO", true, "mark.zuckerberg@facebook.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/50.jpg", "Edwardo Saverin", "CFO", true, "edwardo.saverin@facebook.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/51.jpg", "Dustin Moskotvitz", "CTO", false, "dustin.moskovitz@facebook.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Facebook", "DEV", false, "dev.facebook@facebook.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@facebook.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@facebook.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> TwitterContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/49.jpg", "Evan Williams", "CEO", true, "evan.williams@twitter.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/50.jpg", "Jack Dorsey", "CFO", true, "jack.dorsey@twitter.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/51.jpg", "Noah Glass", "CTO", false, "noah.glass@twitter.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Twitter", "DEV", false, "dev.twitter@twitter.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@twitter.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@twitter.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> NetflixContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/55.jpg", "Reed Hasting", "CEO", true, "reed.hasting@netflix.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "CFO Netflix", "CFO", true, "cfo.netflix@netflix.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/57.jpg", "Mark Randolph", "CTO", false, "noah.glass@netflix.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Netflix", "DEV", false, "dev.netflix@netflix.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@netflix.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@netflix.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> UberContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/61.jpg", "Travis Kalanick", "CEO", true, "travis.kalanick@uber.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/62.jpg", "Garret Camp", "CFO", true, "garret.camp@uber.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "CTO Uber", "CTO", false, "cto.uber@uber.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Uber", "DEV", false, "dev.uber@uber.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@uber.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@uber.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> QanawatContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/85.jpg", "Mohammad Saadieh", "CEO", true, "mohammad.saadieh@qanawat.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/86.jpg", "Mohammad Jawad", "CFO", true, "mohammad.jawad@qanawat.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/87.jpg", "Mohammad Khattab", "CTO", false, "mohammad.khattab@qanawat.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer Qanawat", "DEV", false, "dev.qanawat@qanawat.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@qanawat.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@qanawat.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> airBnbContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/67.jpg", "Brian Chesky", "CEO", true, "brian.chesky@airbnb.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/68.jpg", "Joe Gebbia", "CFO", true, "joe.gebbia@airbnb.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/69.jpg", "Laurence Tosi", "CTO", false, "laurence.tosi@airbnb.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer airBnb", "DEV", false, "dev.airbnb@airbnb.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@airbnb.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@airbnb.com", "0018", "0018"));
            }};

            ArrayList<ContactsInfo> alibabaContacts = new ArrayList<ContactsInfo>(){{
                add(new ContactsInfo(0, "http://stage.4genies.com/media/uploaded-images/pics/73.jpg", "Jack Ma", "CEO", true, "jack.ma@alibaba.com", "0013", "0013"));
                add(new ContactsInfo(1, "http://stage.4genies.com/media/uploaded-images/pics/74.jpg", "Bruce Lee", "CFO", true, "bruce.lee@alibaba.com", "0014", "0014"));
                add(new ContactsInfo(2, "http://stage.4genies.com/media/uploaded-images/pics/75.jpg", "Jacky Chan", "CTO", false, "jacky.chan@alibaba.com", "0015", "0015"));
                add(new ContactsInfo(3, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Developer alibaba", "DEV", false, "dev.alibaba@alibaba.com", "0016", "0016"));
                add(new ContactsInfo(4, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Business Development", "BUS", true, "bus.dev@alibaba.com", "0017", "0017"));
                add(new ContactsInfo(5, "http://stage.4genies.com/media/uploaded-images/pics/q.png", "Marketing Director", "MKT", true, "mkt.dct@alibaba.com", "0018", "0018"));
            }};

            CompaniesInformation AiiACompany = new CompaniesInformation("Marwan Hakim", AiiAContacts,
                    "applle@itunes", "design & architect solutions for the daily use, solutions that will fulfill a need, \n" +
                    "breach a gap or improve the quality of a certain service.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 0);
            CompaniesInformation MicrosoftCompany = new CompaniesInformation("Bill Gates", MicrosoftContacts,
                    "applle@itunes", "At Microsoft our mission and values are to help people and businesses throughout the world realize their full potential.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 1);
            CompaniesInformation GoogleCompany = new CompaniesInformation("Eric Schmidt", GoogleContacts,
                    "applle@itunes", "Search the world's information, including webpages, images, videos and more.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 2);
            CompaniesInformation AppleCompany = new CompaniesInformation("Steve Jobs", AppleContacts,
                    "applle@itunes", "Apple leads the world in innovation with iPhone, iPad, Mac, Apple Watch, iOS, OS X.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 3);
            CompaniesInformation YotubeCompany = new CompaniesInformation("Samsung Korea", YouTubeContacts,
                    "applle@itunes", "Enjoy the videos and music, upload original content, and share it all on YouTube.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 4);
            CompaniesInformation airBnbCompany = new CompaniesInformation("Brian Chesky", airBnbContacts,
                    "applle@itunes", "With over two million unique, well-reviewed properties, we’re sure you’ll find the perfect home base for your next trip. Whether it’s with family, friends, or just work.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 5);
            CompaniesInformation AliBabaCompany = new CompaniesInformation("Brian Chesky", alibabaContacts,
                    "applle@itunes", "The World’s Leading Platform\n" +
                    "for Global Trade", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 6);
            CompaniesInformation AmazonCompany = new CompaniesInformation("Brian Chesky", AmazonContacts,
                    "applle@itunes", "Online retailer of books, movies, music and games along with electronics, toys, apparel, sports, tools, groceries.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 7);
            CompaniesInformation EbayCompany = new CompaniesInformation("Brian Chesky", EbayContacts,
                    "applle@itunes", "What we do\n" +
                    "eBay is where the world goes\u0003 to shop, sell, and give.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 8);
            CompaniesInformation FacebookCompany = new CompaniesInformation("Brian Chesky", FacebookContacts,
                    "applle@itunes", "Facebook helps you connect and share with the people in your life.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 9);
            CompaniesInformation NetflixCompany = new CompaniesInformation("Brian Chesky", NetflixContacts,
                    "applle@itunes", "See what's next.\n" +
                    "\n" +
                    "Watch anywhere. Cancel anytime.\n", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 10);
            CompaniesInformation QanawatCompany = new CompaniesInformation("Brian Chesky", QanawatContacts,
                    "applle@itunes", "Our broad experience across all digital platforms equips us to be able to maximise the profitability of our partners.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 11);
            CompaniesInformation TwitterCompany = new CompaniesInformation("Brian Chesky", TwitterContacts,
                    "applle@itunes", "Connect with your friends — and other fascinating people.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 12);
            CompaniesInformation UberCompany = new CompaniesInformation("Brian Chesky", UberContacts,
                    "applle@itunes", "Uber is the smartest way to get around. One tap and a car comes directly to you.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 13);
            CompaniesInformation YahooCompany = new CompaniesInformation("Brian Chesky", YahooContacts,
                    "applle@itunes", "Yahoo is a guide focused on informing,\n" +
                    "connecting, and entertaining our users.", "",
                    "9661537224", "https://www.youtube.com/watch?v=62hQpiPEquI", false, 14);

            MainActivity.All_COMPANIES_INFO.put(0, AiiACompany);
            MainActivity.All_COMPANIES_INFO.put(1, MicrosoftCompany);
            MainActivity.All_COMPANIES_INFO.put(2, GoogleCompany);
            MainActivity.All_COMPANIES_INFO.put(3, AppleCompany);
            MainActivity.All_COMPANIES_INFO.put(4, YotubeCompany);
            MainActivity.All_COMPANIES_INFO.put(5, airBnbCompany);
            MainActivity.All_COMPANIES_INFO.put(6, AliBabaCompany);
            MainActivity.All_COMPANIES_INFO.put(7, AmazonCompany);
            MainActivity.All_COMPANIES_INFO.put(8, EbayCompany);
            MainActivity.All_COMPANIES_INFO.put(9, FacebookCompany);
            MainActivity.All_COMPANIES_INFO.put(10, NetflixCompany);
            MainActivity.All_COMPANIES_INFO.put(11, QanawatCompany);
            MainActivity.All_COMPANIES_INFO.put(12, TwitterCompany);
            MainActivity.All_COMPANIES_INFO.put(13, UberCompany);
            MainActivity.All_COMPANIES_INFO.put(14, YahooCompany);
        }

        private void loadFavourites()
        {
            try {
                FileInputStream fIn = getApplicationContext().openFileInput("favcompanies");
                int c;
                String temp = "";
                while( (c = fIn.read()) != -1){
                    temp = temp + Character.toString((char)c);
                }
                //string temp contains all the data of the file.
                fIn.close();
                Gson gson = new Gson();
                TypeToken<HashMap<Integer, favourites>> list = new TypeToken<HashMap<Integer, favourites>>(){};
                MainActivity.ALL_COMPANIES_FAV = gson.fromJson(temp, list.getType());
                //Toast.makeText(getApplicationContext(), "Size - "+ALL_COMPANIES_FAV.size(), Toast.LENGTH_SHORT).show();
                if(MainActivity.ALL_COMPANIES_FAV.size() >= 1)
                {
                    MainActivity.mfavBtnCount = MainActivity.ALL_COMPANIES_FAV.size();
                    //Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
