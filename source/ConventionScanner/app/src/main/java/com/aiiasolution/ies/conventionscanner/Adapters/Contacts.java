package com.aiiasolution.ies.conventionscanner.Adapters;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aiiasolution.ies.conventionscanner.Helpers.VolleyImageRequest;
import com.aiiasolution.ies.conventionscanner.SharedContactsActivity;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aiiasolution.ies.conventionscanner.MainActivity;
import com.aiiasolution.ies.conventionscanner.Models.ContactsInfo;
import com.example.ies.conventionscanner.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by IES on 21/07/2016.
 */
public class Contacts extends ArrayAdapter<ContactsInfo> {


    private Integer CompanyID;
    private ImageLoader mImageLoader;

    public Contacts(Context context, ArrayList<ContactsInfo> allcontacts, Integer CompanyID) {
        super(context, 0, allcontacts);
        this.CompanyID = CompanyID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.contacts_adapter_view, parent, false);
        }

        //Init Views
        LinearLayout llLinearLayout = (LinearLayout) convertView.findViewById(R.id.llContactsAdapter);
        final NetworkImageView nivImage = (NetworkImageView) convertView.findViewById(R.id.nvContactsAdapter);
        ProgressBar pbProgressImage = (ProgressBar) convertView.findViewById(R.id.pbContactsImage);
        TextView tvContactsName = (TextView) convertView.findViewById(R.id.tvContactsNameAdapter);
        TextView tvContactsTitle = (TextView) convertView.findViewById(R.id.tvContactsTitleAdapter);

        //Init Values
        tvContactsName.setText(getItem(position).getContactName());
        tvContactsTitle.setText(getItem(position).getContactTitle());
        setImageUrl(nivImage, getItem(position).getContactPic(), pbProgressImage);
        pbProgressImage.getIndeterminateDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);

        if(getItem(position).getContactSync()) llLinearLayout.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.network_imageview_borders_main, getContext().getTheme()));
        else llLinearLayout.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.network_imageview_borders, getContext().getTheme()));

        llLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getItem(position).getContactSync()) syncContact(position, nivImage);
                else Toast.makeText(getContext(), "cannot sync contact, request sent to sync", Toast.LENGTH_SHORT).show();
            }
        });


        return  convertView;
    }

    private void syncContact(int position, NetworkImageView v)
    {
        Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.COMPANY, MainActivity.ALL_COMPANIES.get(CompanyID).getName());
        intent.putExtra(ContactsContract.Intents.Insert.NAME, getItem(position).getContactName());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, getItem(position).getContactPhone());
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, getItem(position).getContactEmail());
        intent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, getItem(position).getContactTitle() + " at " + MainActivity.ALL_COMPANIES.get(CompanyID).getName());
        intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE, getItem(position).getContactPhone2());

        try {
        ArrayList<ContentValues> data = new ArrayList<ContentValues>();
        ContentValues row = new ContentValues();
        row.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE);
        Bitmap mBitmap = ((BitmapDrawable)v.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.PNG , 75, stream);
        row.put(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray());
        data.add(row);
        intent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, data);
            stream.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ((SharedContactsActivity)getContext()).startActivityForResult(intent, 1);
    }

    private void syncContact_Test(int position, NetworkImageView v)
    {
        ArrayList<ContentProviderOperation> ops =
                new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();

// intial setup
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, "Test")
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, getItem(position).getContactName())
                .build());

// add name with rawId backreference
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, getItem(position).getContactName())
                .build());

// add the photo
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap mBitmap = ((BitmapDrawable)v.getDrawable()).getBitmap();
        if(mBitmap != null){    // If an image is selected successfully
            mBitmap.compress(Bitmap.CompressFormat.PNG , 75, stream);

            // Adding insert operation to operations list
            // to insert Photo in the table ContactsContract.Data
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, getItem(position).getContactID())
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO,stream.toByteArray())
                    .build());

            try {
                stream.flush();
            }catch (IOException e) {
                e.printStackTrace();
            }

        try {
            getContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
        }
    }
    private void setImageUrl(final NetworkImageView givenImage, String givenUrl, final ProgressBar pbImage)
    {
        mImageLoader = VolleyImageRequest.getInstance(getContext()).getImageLoader();
        mImageLoader.get(givenUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap() != null) {
                    pbImage.setVisibility(View.GONE);
                    givenImage.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        givenImage.setImageUrl(givenUrl, mImageLoader);
    }



}
