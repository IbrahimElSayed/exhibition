package com.aiiasolution.ies.conventionscanner;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothScannerThreadSdkVersionMin19Max20;
import com.aiiasolution.ies.conventionscanner.Fragments.mainFragment;
import com.aiiasolution.ies.conventionscanner.Fragments.favoritesFragment;
import com.aiiasolution.ies.conventionscanner.Bluetooth.BluetoothScannerThreadSdkVersionMin21;
import com.aiiasolution.ies.conventionscanner.Models.Companies;
import com.aiiasolution.ies.conventionscanner.Models.CompaniesInformation;
import com.aiiasolution.ies.conventionscanner.Models.favourites;
import com.example.ies.conventionscanner.R;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity{

    public static TextView favBtn;
    public static Boolean vViewFavorites = false;
    public static Integer mfavBtnCount = 0;

    public static Boolean TESTING_CSV = false;

    /* ---- public properties ---- */
    public static final ArrayList<String> FILTER_ADDRESS = new ArrayList<>();
    public static final HashMap<Integer, Companies> ALL_COMPANIES = new HashMap<>();
    public static final HashMap<Integer, CompaniesInformation> All_COMPANIES_INFO = new HashMap<>();
    public static HashMap<Integer, favourites> ALL_COMPANIES_FAV = new HashMap<>();

    public static TelephonyManager telephonyManager;
    /* ---- End public properties ---- */

    private mainFragment mMainFrag;
    private Fragment mFragment;
    private Integer mFragmentIndex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            initValues();
            initViews();
            showFragment(1);
    }

    private void initValues()
    {
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    }

    private void initViews()
    {
        getSupportActionBar().setTitle("");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void btnIntervalClicked()
    {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(60, 0, 60, 0);
        final EditText input = new EditText(this);
        layout.addView(input, params);
        input.setHint("Enter 0 for real time scanning");
        ad.setTitle("Scan Interval in seconds");
        ad.setView(layout);
        input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        ad.setNegativeButton("Cancel", null);
        ad.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Double getNum = Double.parseDouble( input.getText().toString() );
                if(getNum != 0) {
                    Toast.makeText(getApplicationContext(), "Scan changed to " + getNum + " seconds", Toast.LENGTH_SHORT).show();
                    getNum *= 1000;
                }else
                {
                    Toast.makeText(getApplicationContext(), "Real time scanning enabled", Toast.LENGTH_SHORT).show();
                    getNum = 300.0;
                }
                BluetoothScannerThreadSdkVersionMin19Max20.updatingPeriodMilliSeconds = getNum.intValue();
                BluetoothScannerThreadSdkVersionMin21.updatingPeriodMilliSeconds = getNum.intValue();
            }
        });
        ad.create();
        ad.show();
    }

    private void viewFavourites()
    {
        vViewFavorites = true; showFragment(2);
    }


    public void showFragment(int fragmentNumber)
    {
        mFragmentIndex = fragmentNumber;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(fragmentNumber == 1) {
            mFragment = new mainFragment();
            mMainFrag = (mainFragment)mFragment;
        }
        if(fragmentNumber == 2) {
            mFragment = new favoritesFragment();
        }
        fragmentTransaction.replace(R.id.gridViewFrags, mFragment).addToBackStack(null).commit();
    }

//Override functions
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode)
        {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        mMainFrag.startBluetoothScan();
                } else {
                    Toast.makeText(getApplicationContext(), "Application cannot start without permissions.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        MenuItem count = menu.findItem(R.id.action_favorites);
        MenuItemCompat.setActionView(count, R.layout.badge_layout);
        View view = MenuItemCompat.getActionView(count);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!vViewFavorites)
                    viewFavourites();
            }
        });
        favBtn = (TextView) view.findViewById(R.id.notif_count_badge);
        favBtn.setVisibility(View.GONE);
        if(mfavBtnCount >= 1)
        {
            favBtn.setVisibility(View.VISIBLE);
            favBtn.setText(String.valueOf(mfavBtnCount));
        }

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id)
        {
            case R.id.action_settings:
                btnIntervalClicked();
                return true;
            case R.id.action_connectedpeople:
                return true;
            case R.id.action_favorites:
                //Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();
                //viewFavourites();
                return true;
            case R.id.action_meeting:
                return true;
            case R.id.action_mapsposition:
                return true;
            default:
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if(mFragmentIndex == 1) {
            moveTaskToBack(true);
        }else
        if(mFragmentIndex == 2) {
            mFragmentIndex = 1;
            vViewFavorites = false;
            super.onBackPressed();
        } else
            super.onBackPressed();
    }
    @Override
    protected void onDestroy() {
        mMainFrag.bluetoothScannerThread.endFlag = true;
        mMainFrag.bluetoothScannerThread.terminate();
        super.onDestroy();
    }
}
